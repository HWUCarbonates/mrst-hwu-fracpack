classdef MechanicsSolver
    %FracturedMesh Mesh with duplicated nodes and edges in fracture
    
    properties
        mesh
        E
        nu
        ubc
        fbc
        S
        rhs
        vemextra
    end
    
    methods
        
        function obj = MechanicsSolver(mesh)
            
            obj.mesh = mesh;
            obj.ubc = struct('nodes', [], 'uu', [], 'mask', []);
            obj.fbc = struct('faces', [], 'force', []);
            obj.S = [];
            obj.rhs = [];
                             
        end
        
        function obj = set_elastic_parameters(obj,E,nu)
            obj.E = E;
            obj.nu = nu;
        end
        
        function [ecpr,extra] = get_contact_pressure(obj,varargin)
            
            opt = struct('interpolation_method', 'avg');
            opt = merge_options(opt, varargin{:});
            
            %% Setup Mechanics
            obj = obj.setup_mechanics();
            
            % Adding fracture body nodes
            master_nodes = [];
            slave_nodes = [];
            for i = 1:length(obj.mesh.fracnet)
                mnodes = obj.mesh.fracnet{i}.master_nodes;
                snodes = obj.mesh.fracnet{i}.slave_nodes;
                master_nodes = [master_nodes; mnodes];
                slave_nodes = [slave_nodes; snodes];    
            end
            
%             all_int_nodes = obj.mesh.intersections.global_lists.nodes;
            to_exclude = [];
            for i = 1:length(obj.mesh.intersections.frac_frac)
                int = obj.mesh.intersections.frac_frac{i};
                last_node = int.nodes(end);
                idxm = find(master_nodes==last_node);
                idxs = find(slave_nodes==last_node);
                if(~isempty(idxm))
%                     to_exclude = [to_exclude;idxm(end)];
                    master_nodes(idxm(end)) = [];
                    slave_nodes(idxm(end)) = [];
                else
                    master_nodes(idxs(end)) = [];
                    slave_nodes(idxs(end)) = [];
%                     to_exclude = [to_exclude;idxs(end)];
                end
                    
            end
%             disp(to_exclude)
            %% build lagrang system 
            gap = zeros(2*length(master_nodes),1);
            [Saug,rhsaug] = obj.add_restrictions(master_nodes,...
                                                  slave_nodes,...
                                                  gap);

            obj.S = Saug;
            obj.rhs = rhsaug;

            [extra.u,extra.s,extra.e,lambda] = ...
                                         obj.solve(2*length(master_nodes));
            extra.ncpr = obj.get_node_contact_pressure(master_nodes,...
                                                 slave_nodes,...
                                                 lambda);
            ecpr = obj.get_edge_contact_pressure(extra.ncpr,...
                                                'interpolation_method',...
                                                opt.interpolation_method);
            
        end
        
        function [ncpr] = get_node_contact_pressure(obj, master_nodes, slave_nodes, lambda)

            %% We consider only nodes that are not lying in intersections
%             [master_nodes,iam] = setdiff(master_nodes,...
%                             obj.mesh.intersections.global_lists.nodes,...
%                             'stable');
%             [slave_nodes,ias] = setdiff(slave_nodes,...
%                             obj.mesh.intersections.global_lists.nodes,...
%                             'stable');
            lambda = reshape(lambda',2,length(lambda)/2)';
%             lambda = lambda(iam,:);
            edge_nodes = obj.mesh.utils.edgenodes;
            ncpr = zeros(obj.mesh.G.nodes.num,1);

            for i = 1:length(master_nodes)
                edges = find(edge_nodes(:,1)==master_nodes(i) |...
                             edge_nodes(:,2)==master_nodes(i));
                N = obj.mesh.G.faces.neighbors(edges,:);
                edges = edges(find(N(:,1)==0 | N(:,2)==0));

                ns = obj.mesh.G.faces.normals(edges,:);
                ns(1,:) = ns(1,:)/norm(ns(1,:));
                ns(2,:) = ns(2,:)/norm(ns(2,:));
                l = lambda(i,:);
                dotavg = 0.5*(abs(dot(l,ns(1,:)))+...
                              abs(dot(l,ns(2,:))));
                areas = obj.mesh.G.faces.areas(edges,:);
                areaavg = mean(areas);
                cpress = dotavg/areaavg;
                ncpr(master_nodes(i)) = cpress;
                ncpr(slave_nodes(i)) = cpress;
            end

        end
        
        function [ecpr] = get_edge_contact_pressure(obj, ncpr, varargin)
            
            opt = struct('interpolation_method', 'avg');
            opt = merge_options(opt, varargin{:});
            
            ecpr = [];
            if(strcmp(opt.interpolation_method,'avg'))
                % we fill the master nodes with '0' cpress with the
                % neighbor value
                for i = 1:length(obj.mesh.fracnet)
                    mnodes = obj.mesh.fracnet{i}.master_nodes; 
                    master_cpress = ncpr(mnodes);
                    idx = find(master_cpress==0);
                    if(~isempty(idx))
                        x = (1:length(master_cpress));
                        v = master_cpress;
                        x(idx) = [];
                        v(idx) = [];
                        cpr_interp = interp1(x,v,idx);
                        ncpr(mnodes(idx)) = cpr_interp;
                    end
                end
            end
            if(strcmp(opt.interpolation_method,'max'))
                % we fill the master nodes with '0' cpress with the
                % neighbor value
                for i = 1:length(obj.mesh.fracnet)
                    mnodes = obj.mesh.fracnet{i}.master_nodes; 
                    master_cpress = ncpr(mnodes);
                    idx = find(master_cpress==0);
                    if(~isempty(idx))
                        x = (1:length(master_cpress));
                        ncpr(mnodes(idx)) = max(master_cpress);
%                         ncpr(mnodes(idx)) = -10*mega;
                    end
                end
            end
            if(any(isnan(ncpr)))
               warning('There are nans in ncpr !!! TAKE CARE !!!');
            end
            
            for i = 1:length(obj.mesh.fracnet)
                medges = obj.mesh.fracnet{i}.master_edges; 
                for j = 1:length(medges)
                    nodes = obj.mesh.utils.edgenodes(medges(j),:);
                    cpr = ncpr(nodes);
                    ecpr(end+1,1) = mean(cpr);
                end
            end

        end
        
        function [Saug,rhsaug] = add_restrictions(obj,master_nodes,slave_nodes,gap)
            
            nextraeqs = 2*length(master_nodes);
            Saug = [obj.S,zeros(size(obj.S, 2), nextraeqs)];
            Saug = [Saug;zeros(nextraeqs, size(Saug, 2))];
            rhsaug = [obj.rhs; -gap];
            ndof = size(obj.S,1);
            % loop over nodes in the fracture
            for j = 1:length(master_nodes)
                % Body forces to existing equations: uim-lambdai=0 and
                % uis+lambdai=0
                % x direction
                Saug(master_nodes(j)*2-1, ndof+2*j-1)  =  1;
                Saug(slave_nodes(j)*2 -1 , ndof+2*j-1)  = -1; 
                % y direction
                Saug(master_nodes(j)*2, ndof+2*j)  =  1;
                Saug(slave_nodes(j)*2 , ndof+2*j)  = -1; 

                % New equations: uim = uis
                % x direction
                Saug(ndof+2*j-1, master_nodes(j)*2-1) =  1;
                Saug(ndof+2*j-1, slave_nodes(j)*2-1) =  -1;
                % y direction
                Saug(ndof+2*j, master_nodes(j)*2) = 1;
                Saug(ndof+2*j, slave_nodes(j)*2) = -1;
            end

        end
        
        function obj = set_displacement_bc(obj, value, direction)
            
            nodes = obj.ubc.nodes;
            values = obj.ubc.uu;
            mask = obj.ubc.mask;
            
            %% domain limits
            x_size = max(obj.mesh.G.nodes.coords(:,1));
            y_size = max(obj.mesh.G.nodes.coords(:,2));

            %% Finding boundary nodes
            left_nodes = find(obj.mesh.G.nodes.coords(:,1)==0);
            right_nodes = find(obj.mesh.G.nodes.coords(:,1)==x_size);
            bottom_nodes = find(obj.mesh.G.nodes.coords(:,2)==0);
            top_nodes = find(obj.mesh.G.nodes.coords(:,2)==y_size);
            
            %% arrays with values and masks
            switch direction
                case 'x'
                    % left
                    vd = value*ones(length(left_nodes),2);
                    vd(:,2) = 0;
                    nd = left_nodes;
                    
                    nodes = [nodes; nd];
                    values = [values; vd];
                    mask = [mask; ones(size(vd))];

                    % right
                    vd = -value*ones(length(right_nodes),2);
                    vd(:,2) = 0;
                    nd = right_nodes;
                    
                    nodes = [nodes; nd];
                    values = [values; vd];
                    mask = [mask; ones(size(vd))];
                    
                case 'y'
                    
                    % top
                    vd = -value*ones(length(top_nodes),2);
                    vd(:,1) = 0;
                    nd = top_nodes;
                    
                    nodes = [nodes; nd];
                    values = [values; vd];
                    mask = [mask; ones(size(vd))];

                    % bottom
                    vd = value*ones(length(bottom_nodes),2);
                    vd(:,1) = 0;
                    nd = bottom_nodes;
                    
                    nodes = [nodes; nd];
                    values = [values; vd];
                    mask = [mask; ones(size(vd))];
                    
            end
            
            obj.ubc.nodes = nodes;
            obj.ubc.uu = values;
            obj.ubc.mask = mask;
        end
        
        function obj = set_no_rotation_bc(obj)
            
            %% Creating MRST grid
            x_size = max(obj.mesh.G.nodes.coords(:,1));
            y_size = max(obj.mesh.G.nodes.coords(:,2));

            % Finding middle nodes
            pleft    = [0,y_size/2];
            pright   = [x_size,y_size/2];
            pbottom  = [x_size/2,0];
            ptop     = [x_size/2,y_size];

            left = bsxfun(@minus, obj.mesh.G.nodes.coords, pleft);
            [~,midleft] = min(sqrt(sum(left.^2,2)));
            right = bsxfun(@minus, obj.mesh.G.nodes.coords, pright);
            [~,midright] = min(sqrt(sum(right.^2,2)));
            bottom = bsxfun(@minus, obj.mesh.G.nodes.coords, pbottom);
            [~,midbottom] = min(sqrt(sum(bottom.^2,2)));
            top = bsxfun(@minus, obj.mesh.G.nodes.coords, ptop);
            [~,midtop] = min(sqrt(sum(top.^2,2)));

            % Displacement
            nodes = [midleft; midright; midbottom; midtop];
            values = zeros(length(nodes),2);
            % We activate 0 displacement only in the x direction for top and bottom
            % and y direction for left and right
            mask = [0,1;0,1;1,0;1,0];
            
            obj.ubc = struct('nodes', nodes, 'uu', values, 'mask', mask);
        end
        
        function obj = set_load_bc(obj,value,direction)
            
            force = obj.fbc.force;
            faces = obj.fbc.faces;
            switch direction
                case 'x'
                    left_faces = find(abs(obj.mesh.G.faces.centroids(:,1))<=1e-3);
                    right_faces = find(abs(obj.mesh.G.faces.centroids(:,1)-obj.mesh.x_size)<=1e-3);
                    
                    % left
                    vf = value*ones(length(left_faces),2);
                    ff = left_faces;
                    vf(:,2) = 0;
                    faces = [faces;ff];
                    force = [force;vf];

                    % right
                    vf = -value*ones(length(right_faces),2);
                    ff = right_faces;
                    vf(:,2) = 0;
                    faces = [faces;ff];
                    force = [force;vf];
                    
                case 'y'
                    bottom_faces = find(abs(obj.mesh.G.faces.centroids(:,2))<=1e-3);
                    top_faces = find(abs(obj.mesh.G.faces.centroids(:,2)-obj.mesh.y_size)<=1e-3);
                    
                    % top
                    vf = -value*ones(length(top_faces),2);
                    ff = top_faces;
                    vf(:,1) = 0;
                    faces = [faces;ff];
                    force = [force;vf];

                    % bottom
                    vf = value*ones(length(bottom_faces),2);
                    ff = bottom_faces;
                    vf(:,1) = 0;
                    faces = [faces;ff];
                    force = [force;vf];
                otherwise
                    error('direction should be either x or y');
            end
            obj.fbc.faces = faces;
            obj.fbc.force = force;
            
        end
        
        function obj = setup_mechanics(obj)
           
            %% Define the rock parameters
            Ev = repmat(obj.E, obj.mesh.G.cells.num, 1);
            nuv = repmat(obj.nu, obj.mesh.G.cells.num, 1);

            %% Linear elasticity tensor
            mrstModule add vemmech
            C = Enu2C(Ev, nuv, obj.mesh.G);
            
            %% BC struct
            bc = struct('disp_bc', obj.ubc, 'force_bc', obj.fbc);
            
            %% Zero load
            load = @(x)repmat([0, 0], size(x, 1), 1);
            
            %% Asseobjling system
            [~, extra] = VEM_linElast(obj.mesh.G, C, bc, load,...
                         'experimental_scaling',true,...
                         'no_solve',true);
            
            %% Assigning A and rhs
            obj.S = extra.S;
            obj.rhs = extra.rhs;
            
            %% Assigning useful vem operators
            obj.vemextra.isdirdofs = extra.disc.isdirdofs;
            obj.vemextra.V_dir = extra.disc.V_dir;
            obj.vemextra.D = extra.D;
            obj.vemextra.WC = extra.WC;
            obj.vemextra.assemb = extra.assemb;
            
        end
        
        function [u,s,e,lambda] = solve(obj,nlambda)
            % Quick remark here: the system may be augmented by 'nlambda'
            % variables. For the scope of this class/function, we don't
            % really care what these lambda variables represent. 
            if(nargin<2)
                nlambda = 0;
            end
            
            %% Eliminating dirichlet nodes
            isdirdofs_aug = [obj.vemextra.isdirdofs;false(nlambda,1)];
            
            % Reduce the degrees of freedom
            b = obj.rhs;
            A = obj.S(~isdirdofs_aug, ~isdirdofs_aug);
            %% Solving the system
            x = A\b;
            
            %% Extracting the last nlambda variables
            lambda = x(end-nlambda+1 : end);
            x(end-nlambda+1 : end) = [];
            
            %% Constructing displacements, strain and stresses
            ndofs = 2*obj.mesh.G.nodes.num;
            uhat   = nan(ndofs, 1);
            uhat(obj.vemextra.isdirdofs)  = obj.vemextra.V_dir(obj.vemextra.isdirdofs);
            uhat(~obj.vemextra.isdirdofs) = x;
            u = reshape(uhat, obj.mesh.G.griddim, [])';
            s = reshape(obj.vemextra.D*obj.vemextra.WC'*...
                        obj.vemextra.assemb'*...
                        reshape(u', [], 1),...
                        3, [])';
            e = reshape(obj.vemextra.WC'*...
                        obj.vemextra.assemb'*...
                        reshape(u', [], 1),...
                        3, [])';
            
        end
        
    end
end

