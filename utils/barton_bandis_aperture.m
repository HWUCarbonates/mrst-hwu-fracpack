function [E,e] = barton_bandis_aperture(cpress, varargin)

    opt = struct('JRC',12,'JCS',120,...
                 'initial_stiffness', 0,...
                 'maximum_closure', 0,...
                 'initial_aperture', 0);
    opt = merge_options(opt, varargin{:});   
    
    assert(opt.maximum_closure<=opt.initial_aperture,...
           'Maximum closure must be less than initial aperture.');
    
    if(~opt.initial_aperture)
        E0 = opt.JRC/50;
    else
        E0 = opt.initial_aperture;
    end
    
    if(~opt.maximum_closure)
        vm = -0.1032 - 0.0074*opt.JRC + 1.135*(opt.JCS/E0)^(-0.251);
    else
        vm = opt.maximum_closure;
    end

    %% Initial Stiffness
    if(~opt.initial_aperture)
        Kni = -7.15 + 1.75*opt.JRC + 0.02*opt.JCS/E0;
    else
        Kni = opt.initial_stiffness;
    end
    
    %% Converting normal stress to MPa
    sigman = cpress/(mega*Pascal);

    %% Normal mechanical aperture (in mm)
    E = E0 - ( 1/vm + Kni./sigman ).^-1;

    %% Converting to micro meters
    Emicro = E*milli/micro;

    %% Hydraulic aperture (in micro meters)
    emicro = (Emicro.^2)/opt.JRC^2.5;

    %% Converting apertures to S.I.
    e = emicro*micro;
    E = E*milli;
    
end

