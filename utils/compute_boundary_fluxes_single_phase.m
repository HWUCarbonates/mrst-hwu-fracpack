function [bc_fluxes] = compute_boundary_fluxes_single_phase(solver, sw, pn)
            
        %% type
        isP = reshape(strcmpi(solver.bc.type, string('p')), [], 1);

        %% Transmissibility and mobility
        mobW = solver.fluid.krw(sw, solver.bc.cells)/solver.fluid_info.muw;
        mobO = solver.fluid.krn(sw, solver.bc.cells)/solver.fluid_info.mun;
        mobT = mobW + mobO;
        
        %% get transmissibility of matrix faces
        T = get_boundary_transmissibilities(solver);

        %% Initializing fluxes
        zeroAD = solver.ad_backend.convertToAD(zeros(length(solver.bc.cells),1),...
                                               mobW);
        qw = zeroAD;

        %% Calculating delta pressure
        dP = solver.bc.values - pn(solver.bc.cells);
        isInj = dP >= 0;

        isPandInj = isP & isInj;
        isPandProd = isP & ~isInj;

        %% In flow
        qw(isPandInj) = mobT(isPandInj).*T(isPandInj).*dP(isPandInj);

        %% Out flow
        qw(isPandProd) = mobT(isPandProd).*T(isPandProd).*dP(isPandProd);
            
        isInj = solver.bc.values > 0;
        isQandInj = ~isP & isInj;
        isQandProd = ~isP & ~isInj;

        %% In flow
        qw(isQandInj) = solver.bc.values(isQandInj);

        %% Out flow
        f = mobW(isQandProd)./mobT(isQandProd);
        qw(isQandProd) = f.*solver.bc.values(isQandProd);

        bc_fluxes = qw;

end
