function [ T ] = get_boundary_transmissibilities( solver )
   
    T = [];
    for i = 1:length(solver.bc.cells)
        medium = solver.bc.medium(i);

        if(strcmp(medium,'m'))

            face = solver.bc.faces(i);

            %% get transmissibility of matrix faces
            T_i = solver.T_all(face);

        elseif(strcmp(medium,'f'))
            if(solver.fracture_only)
                ind = solver.bc.cells(i);
            else
                [~,ind] = ismember(solver.bc.cells(i), solver.virtual_cells.global_ids);
            end
            T_i = solver.half_trans.fracfrac(ind);
        else
            error('Inside get_boundary_transmissibilities: medium should be "f" or "m"');
        end
        T(end+1,1) = T_i;
    end

end

