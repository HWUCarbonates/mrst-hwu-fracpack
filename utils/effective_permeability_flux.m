function [k] = effective_permeability_flux(solver, bc_fluxes, direction, dp)
        
    G = solver.mesh.G;
    x_size = max(G.nodes.coords(:,1));
    y_size = max(G.nodes.coords(:,1));
    
    if(strcmp(direction,'y'))
        L = y_size;
        A = x_size;
    else
        A = y_size;
        L = x_size;
    end     
    
    qw = bc_fluxes.val;
    qw = sum(qw(qw > 0));
    
    muw = solver.fluid_info.muw;
    
    k = ((muw*qw)/A)*(L/dp);
    
end

