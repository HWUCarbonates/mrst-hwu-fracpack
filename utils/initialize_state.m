function [ state ] = initialize_state( solver, sw, pn )

    %% Total number of cells
    if(~solver.fracture_only)
        nc = solver.mesh.G.cells.num;
    else
        nc = 0;
    end
    nc = nc + solver.virtual_cells.num;

    %% Initializing arrays
    if(length(sw)==1)
        state.sw = sw*ones(nc, 1);
    else
        state.sw = sw;
    end
    if(length(pn)==1)
        state.pn = pn*ones(nc, 1);
    else
        state.pn = pn;
    end
    

end

