function [ data ] = read_abaqus_node_data(mesh, path, prefix )

    %% Checking if source is abaqus mesh
%     assert(strcmp(mesh.mesh_source,'abaqus'),'Mesh source should be Abaqus.');

    %% filename
    filename = [prefix '_node_data.txt'];

    filepath = [path filename];
    ab_data = dlmread(filepath);

    %% Get nodal values
    nodes = ab_data(:,1);
    ux = ab_data(:,2);
    uy = ab_data(:,3);
    sxx = ab_data(:,4);
    syy = ab_data(:,5);

    %% Output
    data.all_nodes = nodes;
    data.values.u = [ux,uy];
    data.values.s = [sxx,syy];
    
end

