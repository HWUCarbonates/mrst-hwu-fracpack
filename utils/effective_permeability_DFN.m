function [k, A1, A2, A] = effective_permeability_DFN(solver, bc_fluxes, direction,tol,fprops,dp)
        
    G = solver.mesh.G;
    x_size = max(G.nodes.coords(:,1));
    y_size = max(G.nodes.coords(:,1));
    
    if(strcmp(direction,'y'))
        side = 'ymax';
    else
        side = 'xmax'
    end
    
    [cells,faces] = get_boundary_cells( solver, side, 'f',...
                                        'tol',tol);
    A1 = 0;                                
    for i=1:length(cells)
        A1 = A1 + fprops.values.aperture(i);
    end
    
%     size(cells)
    
    if(strcmp(direction,'y'))
        side = 'ymin';
    else
        side = 'xmin'
    end
    
    [cells,faces] = get_boundary_cells( solver, side, 'f',...
                                        'tol',tol);
    A2 = 0;                                
    for i=1:length(cells)
        A2 = A2 + fprops.values.aperture(i);
    end
%     size(cells)
    
    if(strcmp(direction,'y'))
        L = y_size;
        A  = (min(A1,x_size) + min(A2,x_size))/2;
    else
        L = x_size;
        A  = (min(A1,y_size) + min(A2,y_size))/2;
    end
    
    
    qw = bc_fluxes.val;
    qw = sum(qw(qw > 0));
    
    muw = solver.fluid_info.muw;
    
    k = ((muw*qw)/A)*(L/dp);
    
end

