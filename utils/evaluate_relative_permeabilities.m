function [krw, krn] = evaluate_relative_permeabilities(solver, sw,cell)
   
        krw = solver.fluid.krw(sw,cell);
        krn = solver.fluid.krn(sw,cell);
        
end

