function [ data ] = read_abaqus_element_node_data(mesh, path, prefix)

    %% Checking if source is abaqus mesh
%     assert(strcmp(mesh.mesh_source,'abaqus'),'Mesh source should be Abaqus.');

    %% filename
    filename = [prefix '_elem_node_data.txt'];

    filepath = [path filename];
    ab_data = dlmread(filepath);

    %% Ensures that each contact value is appearing twice
    for i=1:2:size(ab_data,1)-1
        assert(ab_data(i,3)==ab_data(i+1,3))
        assert(ab_data(i,4)==ab_data(i+1,4))
        assert(ab_data(i,5)==ab_data(i+1,5))
        assert(ab_data(i,6)==ab_data(i+1,6))
    end

    %% Get nodal values
    elem_data_nodes = ab_data(1:2:size(ab_data,1),3);
    cpress = ab_data(1:2:size(ab_data,1),4);

    cp_frac = cpress~=0;
    elem_data_nodes = elem_data_nodes(cp_frac);
    cpress = cpress(cp_frac);

    %% Map results to whole node array
    cpress_node_data = zeros(mesh.G.nodes.num,1);
    cpress_node_data(elem_data_nodes) = cpress;

    data.values.cpress = [];
    data.master_edges = [];

    %% Building fracture prop cell array
    for i = 1:length(mesh.fracnet)
        fraci = mesh.fracnet{i};
        medges = fraci.master_edges;

        cpress = [];
        for j = 1:length(medges)
            edgej = medges(j);
            nodes = mesh.utils.edgenodes(edgej,:);
            cpress(end+1,1) = (cpress_node_data(nodes(1)) + cpress_node_data(nodes(2)))/2;
        end

        %% First and last edges
        cpress(1,1) = cpress(2,1);
        cpress(end,1) = cpress(end-1,1);

        data.values.cpress = [data.values.cpress; cpress];
        data.master_edges = [data.master_edges; medges];
    end


end