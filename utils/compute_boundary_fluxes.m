function [bc_fluxes] = compute_boundary_fluxes(solver, sw, pn)
            
        %% type
        isP = reshape(strcmpi(solver.bc.type, string('p')), [], 1);

        %% inlet saturations
        sat_ph1 = solver.bc.sat(:,1);
        sat_ph2 = solver.bc.sat(:,2);

        %% Transmissibility and mobility
        mobW = solver.fluid.krw(sw, solver.bc.cells)/solver.fluid_info.muw;
        mobO = solver.fluid.krn(sw, solver.bc.cells)/solver.fluid_info.mun;
        mobT = mobW + mobO;
        
        %% get transmissibility of matrix faces
        T = get_boundary_transmissibilities(solver);

        %% Initializing fluxes
        zeroAD = solver.ad_backend.convertToAD(zeros(length(solver.bc.cells),1),...
                                               mobW);
        [qw, qo] = deal(zeroAD);

        %% P
        dP = solver.bc.values - pn(solver.bc.cells);
        isInj = dP >= 0;
        isPandInj = isP & isInj;
        isPandProd = isP & ~isInj;

        %% In flow P
        qw(isPandInj) = sat_ph1(isPandInj).*mobT(isPandInj).*T(isPandInj).*dP(isPandInj);
        qo(isPandInj) = sat_ph2(isPandInj).*mobT(isPandInj).*T(isPandInj).*dP(isPandInj);

        %% Out flow P
        qw(isPandProd) = mobW(isPandProd).*T(isPandProd).*dP(isPandProd);
        qo(isPandProd) = mobO(isPandProd).*T(isPandProd).*dP(isPandProd);

        %% Q
        isInj = solver.bc.values > 0;
        isQandInj = ~isP & isInj;
        isQandProd = ~isP & ~isInj;

        %% In flow Q
        qw(isQandInj) = sat_ph1(isQandInj).*solver.bc.values(isQandInj);
        qo(isQandInj) = sat_ph2(isQandInj).*solver.bc.values(isQandInj);
        
        %% Out flow Q
        fw = mobW(isQandProd)./mobT(isQandProd);
        fo = mobO(isQandProd)./mobT(isQandProd);
        qw(isQandProd) = fw.*solver.bc.values(isQandProd);        
        qo(isQandProd) = fo.*solver.bc.values(isQandProd);      
        
        bc_fluxes = {qw,qo};

end

