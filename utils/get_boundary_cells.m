function [ cells,faces ] = get_boundary_cells( solver, side, medium, varargin )
    
    opt = struct('tol', 1*meter);
    opt = merge_options(opt, varargin{:});

    G = solver.mesh.G;
    
    [xmin,ymin] = deal(min(G.nodes.coords(:,1)),...
                       min(G.nodes.coords(:,2)));
    [xmax,ymax] = deal(max(G.nodes.coords(:,1)),...
                       max(G.nodes.coords(:,2)));
                   
    if(strcmp(medium,'m'))
        
        if(strcmp(side,'xmin'))
            faces = find(abs(G.faces.centroids(:,1)-xmin)<=1e-05);
        elseif(strcmp(side,'xmax'))
            faces = find(abs(G.faces.centroids(:,1)-xmax)<=1e-05);
        elseif(strcmp(side,'ymin'))
            faces = find(abs(G.faces.centroids(:,2)-ymin)<=1e-05);
        elseif(strcmp(side,'ymax'))
            faces = find(abs(G.faces.centroids(:,2)-ymax)<=1e-05);
        end

        N = G.faces.neighbors(faces,:);
        cells = sum(N, 2);
        
    elseif(strcmp(medium,'f'))
        
        boundary_edges = [];
        for i = 1:length(solver.mesh.fracnet)
            
            fraci = solver.mesh.fracnet{i};
            % Coordinates of fracture endpoints
            na = fraci.end_points(1,:);
            nb = fraci.end_points(2,:);
            
            if(strcmp(side,'xmin'))
                if(na(1) <= xmin + opt.tol)
                    boundary_edges(end+1,1) = fraci.master_edges(1);
                elseif(nb(1) <= xmin + opt.tol)
                    boundary_edges(end+1,1) = fraci.master_edges(end);
                end
            elseif(strcmp(side,'xmax'))
                if(na(1) >= xmax - opt.tol)
                    boundary_edges(end+1,1) = fraci.master_edges(1);
                elseif(nb(1) >= xmax - opt.tol)
                    boundary_edges(end+1,1) = fraci.master_edges(end);
                end
            elseif(strcmp(side,'ymin'))
                if(na(2) <= ymin + opt.tol)
                    boundary_edges(end+1,1) = fraci.master_edges(1);
                elseif(nb(2) <= ymin + opt.tol)
                    boundary_edges(end+1,1) = fraci.master_edges(end);
                end
            elseif(strcmp(side,'ymax'))
                if(na(2) >= ymax - opt.tol)
                    boundary_edges(end+1,1) = fraci.master_edges(1);
                elseif(nb(2) >= ymax - opt.tol)
                    boundary_edges(end+1,1) = fraci.master_edges(end);
                end
            end
        end	
        %% At this point, edges contain the master edges set to this 
        % pressure BC. we now find the virtual cells corresponding
        % to these master edges
        [~,indb] = ismember(boundary_edges,...
                            solver.virtual_cells.master_slave_info.medges);
         if(solver.fracture_only)
             cells = indb;
         else
             cells = solver.virtual_cells.global_ids(indb);
         end
         
         %% We set negative faces ids since fractures do not have "faces"
         faces = -1*ones(size(cells));
        
    else
        error('Inside get_boundary_cells: medium should be "f" or "m"');
    end

end

