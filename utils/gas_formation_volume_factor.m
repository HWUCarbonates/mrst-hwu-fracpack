    function [ Bg ] = gas_formation_volume_factor( specific_density, p, T, condensate )
%GasVolumeFactor computes the gas volume factor Bg depending on the
% specific density, pressure and temperature for dry and condensate gases
%                       Bg = p0/T0*Z*T/p;
% Gas fluid properties for dry and condensate gas is taken from Engenharia 
% de Reservatorios de petroleo, A. Jose Rosa et al. 2011
% Explicit calculation for compressibility factor Z taken from
% L. Kareem et al., 'New explicit correlation for the compressibility 
% factor of natural gas: linearized z-factor isotherms', 
% Journal of Petroleum Exploration and Production Technology, 2016

    %%  pressure and temperature at standard condition 
    p0= 1*atm;
    T0 = 288.6 * Kelvin;

    % pseudocritical pressure and temperature
    % correlation after Standing(1951)
    if condensate
        % condensate gas        
        Ppc = (706 - 51.7*specific_density - 11.1*specific_density.^2)*psia;
        Tpc = (187 + 330*specific_density - 71.5*specific_density.^2)*Rankine;
    else
        % dry gas
        Ppc = (677 + 15.0*specific_density - 37.5*specific_density.^2)*psia;
        Tpc = (168 + 325*specific_density - 12.5*specific_density.^2)*Rankine;
    end

    %pseudoreduced pressure and temperature
    Ppr = p/Ppc; 
    Tpr = T/Tpc;
    
    %compressibility factor
    Z = compressibility_factor_KIAM(Tpr,Ppr);
    
    % Gas Formation Volume Factor 
    Bg = p0/T0*Z*T./p;

end


function Z = compressibility_factor_KIAM(Tpr,Ppr)
    % ref: https://link.springer.com/article/10.1007/s13202-015-0209-3
    a1 = 0.317842;
    a2 = 0.382216;
    a3 = -7.768354;
    a4 = 14.290531;
    a5 = 0.000002;
    a6 = -0.004693;
    a7 = 0.096254;
    a8 = 0.166720;
    a9 = 0.966910;
    a10 = 0.0630691;
    a11 = -1.966847;
    a12 = 21.0581;
    a13 = -27.0246;
    a14 = 16.23;
    a15 = 207.783;
    a16 = -488.161;
    a17 = 176.29;
    a18 = 1.88453;
    a19 = 3.05921;

    % single terms
    t = 1/Tpr;
    A = a1*t*exp(a2*(1-t)^2).*Ppr;
    B = a3*t + a4*t^2 + a5*t^6*Ppr.^6;
    C = a9 + a8*t.*Ppr + a7*t^2.*Ppr.^2 + a6*t^3.*Ppr.^3;
    D = a10*t*exp(a11*(1-t)^2);
    E = a12*t + a13*t^2 + a14*t^3;
    F = a15*t + a16*t^2 + a17*t^3;
    G = a18 + a19*t;
    
    y = D.*Ppr./((1+A.^2)./C - A.^2.*B./C.^3);
    
    Z = (D.*Ppr.*(1 + y + y.^2 - y.^3))./((D.*Ppr + E.*y.^2 - F.*y.^G).*(1-y).^3);
end

