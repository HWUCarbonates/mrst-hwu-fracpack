function [ krw, krn ] = relperms( sw, fluid, cells )

    medges = [];
    for i = 1:length(mesh.fracnet)
       medges = [medges; mesh.fracnet{i}.master_edges];
    end

    fprops.master_edges = medges;
    fprops.values.aperture = ap*ones(size(medges));
    fprops.values.permeability = perm*ones(size(medges));
    fprops.values.porosity = poro*ones(size(medges));
    
end

