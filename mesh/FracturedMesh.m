classdef FracturedMesh
    %FracturedMesh Mesh with duplicated nodes and edges in fracture
    
    properties
        mesh_source
        same_line_tol
        fracnet
        intersections
        G
        utils
        x_size
        y_size
        origin
    end
    
    methods
        
        function obj = FracturedMesh(path,prefix,format,varargin)
            
            opt = struct('buffer', [0,0], 'scale_down', 1, 'same_line_tol', 1e-3);
            opt = merge_options(opt, varargin{:}); 
            
            %% Same line tolerance
            obj.same_line_tol = opt.same_line_tol;
            
            %% Mesh source
            obj.mesh_source = format;
            
            %% filenames
            filefrac = [path, prefix, '_fracs.txt'];
            filemesh = [path, prefix, '_mesh.txt'];
            
            %% Reading mesh
            [ele,nodes,obj.origin] = obj.read_mesh(filemesh);
            
            %% Scaling down
            nodes = nodes/opt.scale_down;
            
            %% Reordering nodes for consistent mesh
            [ele,nodes] = obj.renumber2D(ele,nodes);
            
            %% Creating MRST grid
            obj.G = triangleGrid(nodes,ele);
            obj.G = computeGeometry(obj.G);
            
            %% Reading fractures
            if(strcmp(obj.mesh_source,'abaqus'))
                obj.fracnet = obj.read_fracs_abaqus(filefrac);
            else
                if(strcmp(obj.mesh_source,'porepy'))
                    obj.fracnet = obj.read_fracs_porepy(filefrac);
                else
                    error('format should be either abaqus or porepy');
                end
            end
            
            if(sum(opt.buffer)~=0)
                obj = obj.remove_buffer(opt.buffer);
                obj.G = computeGeometry(obj.G); 
            end

            obj.G = createAugmentedGrid(obj.G);
            obj.x_size = max(obj.G.nodes.coords(:,1));
            obj.y_size = max(obj.G.nodes.coords(:,2));
            
            %% The utils struct -  useful information I use all the time
            obj.utils.ele = ele;
            obj.utils.celledges = reshape(obj.G.cells.faces',3,...
                                   length(obj.G.cells.faces)/3)';
            obj.utils.edgenodes = reshape(obj.G.faces.nodes',2,...
                                   length(obj.G.faces.nodes)/2)';
            obj.utils.cellnodes = reshape(obj.G.cells.nodes',3,...
                                   length(obj.G.cells.nodes)/3)';
                               
            %% Computing intersections
            [obj.intersections] = obj.look_for_intersections();

        end
        
        function [obj] = remove_buffer(obj,buffer)
            
            if(sum(buffer)==0)
                return
            end
            
            xmax = max(obj.G.nodes.coords(:,1));
            ymax = max(obj.G.nodes.coords(:,2));
            
            cells_left = find(obj.G.cells.centroids(:,1) < buffer(1));
            obj.G = removeCells(obj.G,cells_left);
            cells_bottom = find(obj.G.cells.centroids(:,2) < buffer(2));
            obj.G = removeCells(obj.G,cells_bottom);
            cells_top = find(obj.G.cells.centroids(:,2)>ymax - buffer(2));
            obj.G = removeCells(obj.G,cells_top);
            cells_right = find(obj.G.cells.centroids(:,1)>xmax - buffer(1));
            obj.G = removeCells(obj.G,cells_right);
            
            nodes = obj.G.nodes.coords;
            
            %% The origin of the mesh
            neworigin = min(nodes,[],1);
            
            %% If origin not (0,0), then translate
            nodes = bsxfun(@minus, nodes, neworigin);
            
            obj.G.nodes.coords = nodes;
            
            %% Now shifting fractures
            for i = 1:length(obj.fracnet)
               xy = obj.fracnet{i}.end_points; 
               xy = bsxfun(@minus, xy, neworigin);
               obj.fracnet{i}.end_points = xy;
            end
            
        end
        
        function [medges] = get_master_edges(obj)
            medges = [];
            for i = 1:length(obj.fracnet)
                medges = [medges;obj.fracnet{i}.master_edges];
            end
            
        end
        
        function [intersections] = look_for_intersections(obj)
            
            %% Intersection struct
            intersections.frac_frac = {};
            intersections.global_lists.edges = [];
            intersections.global_lists.cells = [];
            intersections.global_lists.nodes = [];
            
            %% Nothing to do if we have only one fracture
            if(length(obj.fracnet)==1)
                return;
            end
            
            %% All possible fracture combinations
            combs = nchoosek(1:length(obj.fracnet),2);
            fracs = [];
            points = [];
            for i = 1:size(combs,1)
                fraca = combs(i,1);
                fracb = combs(i,2);
                %% end points
                pa = obj.fracnet{fraca}.end_points(:,:);
                pb = obj.fracnet{fracb}.end_points(:,:);
                point = intersectEdges(createEdge(pa(1,:),pa(2,:)),...
                                       createEdge(pb(1,:),pb(2,:)));

                if(~isnan(point))
                    %% Intersection found. Storing fractures and coordinates
                    fracs(end+1,:) = [fraca,fracb];
                    points(end+1,:) = point;
                end
            end
            
            %% Clustering close intersections
            close_ints = {};
            for i = 1:size(fracs,1)
                aux = [];
                for j = 1:size(fracs,1)
                    if(j~=i)
                        d = norm(points(i,:)-points(j,:));
                        if(d <= 1e-05)
                            aux(end+1) = j;
                        end
                    end
                end
                if(isempty(aux))
                    close_ints{i} = 0;
                else
                    close_ints{i} = aux;
                end
            end
            
            %% Now looping over intersections that are not close
            frac_frac = {};
            for i = 1:size(fracs,1)
                if(~close_ints{i})
                    int.fracs = fracs(i,:);
                    int.coords = points(i,:);
                    frac_frac{end+1} = int;
                end
            end

            %% Now adding intersections with multiple fractures
            frac_frac_multiple = {};
            for i = 1:size(fracs,1)
                closei = close_ints{i};
                if(closei)
                    int.coords = points(i,:);
                    fraci = fracs(i,:);
                    for j = 1:length(closei)
                        fraci = [fraci, fracs(closei(j),:)];
                    end
                    int.fracs = unique(fraci);
                    frac_frac_multiple{end+1} = int;
                end
            end
            
            %% Finally, deleting duplicated intersections
            erase = false(numel(frac_frac_multiple),1);
            for i = 1:length(frac_frac_multiple)
                fracs_i = frac_frac_multiple{i}.fracs;
                for j = i+1:length(frac_frac_multiple)
                   fracs_j =  frac_frac_multiple{j}.fracs;
                   if(fracs_j==fracs_i)
                      erase(j) = true; 
                   end
                end
            end
            frac_frac_multiple(erase) = [];
            
            intersections.frac_frac = [frac_frac,frac_frac_multiple];
        end
        
        function obj = build_master_slave_lists(obj)
            
            %% Build master/slave edges
            obj = obj.build_master_slave_edge_lists();
            
            %% Build master/slave nodes
            obj = obj.build_master_slave_node_lists();
            
            %% Build master/slave cells
            obj = obj.build_master_slave_cell_lists();
            
            %% Processing intersections
%             obj = obj.process_intersections();
            
        end
        
        function obj = process_intersections(obj)
            
            for i = 1:length(obj.intersections.frac_frac)
                
                inti = obj.intersections.frac_frac{i}; 
                point = inti.coords;
               
                %% Finding nodes at intersections
                intnodes = find(abs(obj.G.nodes.coords(:,1)-point(1))<=1e-04 & ...
                               abs(obj.G.nodes.coords(:,2)-point(2))<=1e-04); 
                           
                %% Checking consistency of information
                assert(length(intnodes)/2 == length(inti.fracs));
               
                %% creating cells and nodes
                cells_and_nodes = [];
                for k = 1:length(intnodes)
                  nodek = intnodes(k);
                  cellsk = find(obj.utils.cellnodes(:,1) == nodek | ...
                                obj.utils.cellnodes(:,2) == nodek | ...
                                obj.utils.cellnodes(:,3) == nodek);
                  cells_and_nodes = [cells_and_nodes;...
                                     cellsk, nodek*(ones(length(cellsk),1))];
                end
               
                %% Sorting the cells and nodes clockwise and appending to 
                % intersection
                [~,ind] = obj.sort_clockwise(cells_and_nodes(:,1));
                cells_and_nodes = cells_and_nodes(ind,:);
                inti.cells_and_nodes = cells_and_nodes;
               
                %% appending nodes to intersection
                inti.nodes = unique(cells_and_nodes(:,2),'stable');
               
                %% Finally, finding edges connected to intersection
                edges = [];
                for k = 1:length(cells_and_nodes(:,1))
                   cellk = cells_and_nodes(k,1);
                   edgesk = obj.utils.celledges(cellk,:);
                   N = obj.G.faces.neighbors(edgesk,:);
                   indx = find(N(:,1)==0 | N(:,2)==0);
                   edges = [edges; edgesk(indx)'];
                end
               
                %% appending edges to intersection
                inti.edges = edges;
               
                %% Final assignment
                obj.intersections.frac_frac{i} = inti;
               
                %% The very useful global lists
                obj.intersections.global_lists.cells = ...
                                      [obj.intersections.global_lists.cells;
                                       cells_and_nodes(:,1)];
                obj.intersections.global_lists.nodes = ...
                                      [obj.intersections.global_lists.nodes;
                                       inti.nodes];
                obj.intersections.global_lists.edges = ...
                                      [obj.intersections.global_lists.edges;
                                       inti.edges];
                                    
                
            end
            
        end
        
        function obj = build_master_slave_cell_lists(obj)
            
           for i = 1:length(obj.fracnet)
               
                master_edges = obj.fracnet{i}.master_edges;
                slave_edges = obj.fracnet{i}.slave_edges;
                
                master = [];
                slave = [];
                for j = 1:length(master_edges)
                    master_cell = obj.findElementThatHasEdge(master_edges(j));
                    slave_cell = obj.findElementThatHasEdge(slave_edges(j));
                    master(end+1,1) = master_cell;
                    slave(end+1,1) = slave_cell;
                end
                
                obj.fracnet{i}.master_cells = master;
                obj.fracnet{i}.slave_cells = slave;
                
           end
            
        end
        
        function obj = build_master_slave_node_lists(obj)
            
            for i = 1:length(obj.fracnet)
                
                %% Getting the ids of the first and last fracture points
                pstart = obj.fracnet{i}.end_points(1,:);
                pend = obj.fracnet{i}.end_points(2,:);
                meshpoints = obj.G.nodes.coords;
                astart = bsxfun(@minus,meshpoints,pstart);
                nstart = find(cellfun(@norm,num2cell(astart,2))<=1e-5);
                aend = bsxfun(@minus,meshpoints,pend);
                nend = find(cellfun(@norm,num2cell(aend,2))<=1e-5);

                %% Checking if everything is ok
                assert(length(nstart)==1 & length(nend)==1, 'Inconsistent fracture endpoint');
                endnodes = [nstart,nend];
                
                master_edges = obj.fracnet{i}.master_edges;
                slave_edges = obj.fracnet{i}.slave_edges;
                
                %% Adding all fracture nodes to the list
                master_nodes = [];
                slave_nodes = [];
                for j = 1:length(master_edges)
                    master_nodes = [master_nodes;obj.utils.edgenodes(master_edges(j),:)'];
                    slave_nodes = [slave_nodes;obj.utils.edgenodes(slave_edges(j),:)']; 
                end
                
                %% Sorting master nodes by distance from first fracture point
                pstart = obj.G.nodes.coords(nstart,:);
                points = obj.G.nodes.coords(master_nodes,:);
                a = bsxfun(@minus,points,pstart);
                out = cellfun(@norm,num2cell(a,2));
                [~,indx2] = sort(out);
                master_nodes = master_nodes(indx2);
                
                 %% Sorting slave nodes by distance from first fracture point
                points = obj.G.nodes.coords(slave_nodes,:);
                a = bsxfun(@minus,points,pstart);
                out = cellfun(@norm,num2cell(a,2));
                [~,indx2] = sort(out);
                slave_nodes = slave_nodes(indx2);
                
                master = unique(master_nodes,'stable');
                slave = unique(slave_nodes,'stable');
                
                obj.fracnet{i}.master_nodes = setdiff(master,endnodes,'stable');
                obj.fracnet{i}.slave_nodes = setdiff(slave,endnodes,'stable');
                
            end
        end
        
        function obj = build_master_slave_edge_lists(obj)
            %% Find all fractured edges 
            fracedges = find((obj.G.faces.neighbors(:,1)==0 |...
                              obj.G.faces.neighbors(:,2)==0) &...
                             (obj.G.faces.centroids(:,1) ~= 0) &...
                             (obj.G.faces.centroids(:,1) ~= obj.x_size) &...
                             (obj.G.faces.centroids(:,2) ~= 0) &...
                             (obj.G.faces.centroids(:,2) ~= obj.y_size));
                  
            %% Identify to which fracture they belong
            mfracs = obj.get_mother_fractures(fracedges);
            
            %% Check if any mfracs = 0
            assert(~any(mfracs==0),'Mother fracture = 0');
            
            %% Identify if they are master or slaves
            mors = obj.master_or_slave(fracedges,mfracs);
            
            %% Final loop
            for i = 1:length(obj.fracnet)
                indx = find(mfracs==i); 
                edges = fracedges(indx);
                masterorslave = mors(indx);
                master = edges(find(masterorslave>0));
                slave = edges(find(masterorslave<0));
                
                %% Getting the id of the first fracture point
                pstart = obj.fracnet{i}.end_points(1,:);
                meshpoints = obj.G.nodes.coords;
                a = bsxfun(@minus,meshpoints,pstart);
                nstart = find(cellfun(@norm,num2cell(a,2))<=1e-5);
                
                %% Checking if everything is ok
                assert(length(nstart)==1, 'Inconsistent fracture endpoint');

                %% Sorting master edges by distance from first fracture point
                points = obj.G.faces.centroids(master,:);
                a = bsxfun(@minus,points,pstart);
                out = cellfun(@norm,num2cell(a,2));
                [~,indx2] = sort(out);
                master = master(indx2);
                
                 %% Sorting slave edges by distance from first fracture point
                points = obj.G.faces.centroids(slave,:);
                a = bsxfun(@minus,points,pstart);
                out = cellfun(@norm,num2cell(a,2));
                [~,indx2] = sort(out);
                slave = slave(indx2);
                
                obj.fracnet{i}.master_edges = master;
                obj.fracnet{i}.slave_edges = slave;
            end
        end
        
        function [ele,nodes,origin] = read_mesh(obj, filemesh)
    
            A = dlmread(filemesh);
            nnodes = A(1,1);
            nodes = A(2:nnodes+1,1:2);
            ele = A(nnodes+3:end,:);
            
            %% Porepy numbering starts with 0
            if(min(min(ele))==0)
                ele = ele + 1;
            end
            
            %% The origin of the mesh
            origin = min(nodes,[],1);
            
            %% If origin not (0,0), then translate
            nodes = bsxfun(@minus, nodes, origin);

        end
        
        function [ele,nodes] = renumber2D(obj, ele, nodes)

            for i = 1:length(ele)
               nA = nodes(ele(i,1),:)';
               nB = nodes(ele(i,2),:)';
               nC = nodes(ele(i,3),:)';
               A = [nA,nB,nC;1,1,1];
               area = 0.5*det(A);
               if(area<0)
                   ele(i,:) = ele(i,end:-1:1);
               end
            end

        end
        
        function [fracnet] = read_fracs_porepy(obj, filefrac)
            
            %% Initializing fracture cell array
            fracnet = {};
            
            %% Initializing fracture cell array
            fracnet = {};

            A = dlmread(filefrac);

            i = 1;
            while i <= size(A,1)
                nnodes = A(i);
                end_points = A([i+1,i+nnodes],1:2);
                fracnet{end+1}.end_points = end_points;
                i = i + nnodes + 1;
            end
            
        end
        
        function [fracnet] = read_fracs_abaqus(obj, filefrac)
            
            %% Check if mesh was read
            assert(~isempty(obj.G));
            
            %% Initializing fracture cell array
            fracnet = {};

            A = dlmread(filefrac);

            i = 1;
            while i <= size(A,1)
                nnodes = A(i);
                end_nodes = A(i+1:i+2,1);
                fracnet{end+1}.end_points = obj.G.nodes.coords(end_nodes,:);
                i = i + nnodes + 1;
            end
            
        end
        
        function [mfracs] = get_mother_fractures(obj,fracedges)

            mfracs = zeros(length(fracedges),1);
            for i = 1:length(fracedges)
               for j = 1:length(obj.fracnet)
                   
                    %% The three points
                    xA = obj.fracnet{j}.end_points(1,:);
                    xB = obj.fracnet{j}.end_points(2,:);
                    xT = obj.G.faces.centroids(fracedges(i),:);
                    
                    if(is_point_in_line(xA, xB, xT, obj.same_line_tol))
                       mfracs(i) = j; 
                    end
                           
               end
               
            end
            
        end
        
        function [id] = findElementThatHasEdge(obj,edge)
            
            elementedges = reshape(obj.G.cells.faces',3,length(obj.G.cells.faces)/3)';
            id = find(elementedges(:,1) == edge | ...
                      elementedges(:,2) == edge | ...
                      elementedges(:,3) == edge);
                  
        end
        
        function [cells,ind] = sort_clockwise(obj,cells)
            
            %% Given a bunch of cells, sort them clockwise
            x = obj.G.cells.centroids(cells,1);
            y = obj.G.cells.centroids(cells,2);
            cx = mean(x);
            cy = mean(y);

            a = atan2(y - cy, x - cx);

            [~, ind] = sort(a);
            cells = cells(ind);
            
        end
        
        function [mors] = master_or_slave(obj,fracedges,mfracs)
            
            mors = zeros(length(fracedges),1);
            for i = 1:length(fracedges)
               edgei = fracedges(i);
               elid = findElementThatHasEdge(obj,edgei);
               e = obj.fracnet{mfracs(i)}.end_points;
               eA = e(1,:);
               eB = e(2,:);
               nA = obj.utils.edgenodes(edgei,1);
               nB = obj.utils.edgenodes(edgei,2);
               pA = obj.G.nodes.coords(nA,:);
               pB = obj.G.nodes.coords(nB,:);
               d = dot(eB-eA,pB-pA);
               if(d<0)
                   aux = pB;
                   pB = pA;
                   pA = aux;
               end
               cE = obj.G.cells.centroids(elid,:);

               u = pB - pA;
               v = cE - pA;

               c = cross([u,0],[v,0]);

               if( c(3) > 0 )
                   mors(i)=1;
               else
                   mors(i)=-1;
               end

            end
            
        end
        
    end
        
end

function r = is_point_in_line(xA, xB, xT, tol)

    [x1,y1] = deal(xA(1),xA(2));
    [x2,y2] = deal(xB(1),xB(2));
    [xt,yt] =  deal(xT(1),xT(2));
    
    u = xB-xA;
    v = xT-xA;
    u(3) = 0;
    v(3) = 0;
    
    r1 = (abs(norm(cross(u,v))) <= tol);
    
    r2 = (xt >= min(x1,x2)) &...
         (xt <= max(x1,x2)) &...
         (yt >= min(y1,y2)) &...
         (yt <= max(y1,y2));
     
    r = r1*r2;
       
end


