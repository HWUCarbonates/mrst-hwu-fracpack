%% EXPERIMENTAL CODE !!!!
%

%% 
classdef TortuousFracturedMesh
    %FracturedMesh Mesh with duplicated nodes and edges in fracture
    
    properties
        fracnet
        intersections
        G
        utils
        x_size
        y_size
        origin
    end
    
    methods
        
        function obj = TortuousFracturedMesh(path,prefix,format,varargin)
            
            opt = struct('buffer', [0,0], 'scale_down', 1);
            opt = merge_options(opt, varargin{:}); 
            
            %% filenames
            filefrac = [path, prefix];
            filemesh = [path, prefix, '_mesh.txt'];
            
            %% Reading mesh
            [ele,nodes,obj.origin] = obj.read_mesh(filemesh);
            
            %% Scaling down
            nodes = nodes/opt.scale_down;
            
            %% Reordering nodes for consistent mesh
            [ele,nodes] = obj.renumber2D(ele,nodes);
            
            %% Creating MRST grid
            obj.G = triangleGrid(nodes,ele);
            obj.G = computeGeometry(obj.G);
            
            %% Reading fractures
            if(strcmp(format,'tortuous'))
                obj.fracnet = obj.read_fracs_tortuous(filefrac);
            else
                if(strcmp(format,'porepy'))
                    obj.fracnet = obj.read_fracs_porepy(filefrac);
                else
                    error('format should be either abaqus or porepy');
                end
            end
            
%             obj = obj.remove_buffer(opt.buffer);
%             
%             obj.G = computeGeometry(obj.G); 
            obj.G = createAugmentedGrid(obj.G);
            obj.x_size = max(obj.G.nodes.coords(:,1));
            obj.y_size = max(obj.G.nodes.coords(:,2));
            
            %% The utils struct -  useful information I use all the time
            obj.utils.ele = ele;
            obj.utils.celledges = reshape(obj.G.cells.faces',3,...
                                   length(obj.G.cells.faces)/3)';
            obj.utils.edgenodes = reshape(obj.G.faces.nodes',2,...
                                   length(obj.G.faces.nodes)/2)';
            obj.utils.cellnodes = reshape(obj.G.cells.nodes',3,...
                                   length(obj.G.cells.nodes)/3)';
                               
        end
        
        function [obj] = remove_buffer(obj,buffer)
            
            if(sum(buffer)==0)
                return
            end
            
            xmax = max(obj.G.nodes.coords(:,1));
            ymax = max(obj.G.nodes.coords(:,2));
            
            cells_left = find(obj.G.cells.centroids(:,1) < buffer(1));
            obj.G = removeCells(obj.G,cells_left);
            cells_bottom = find(obj.G.cells.centroids(:,2) < buffer(2));
            obj.G = removeCells(obj.G,cells_bottom);
            cells_top = find(obj.G.cells.centroids(:,2)>ymax - buffer(2));
            obj.G = removeCells(obj.G,cells_top);
            cells_right = find(obj.G.cells.centroids(:,1)>xmax - buffer(1));
            obj.G = removeCells(obj.G,cells_right);
            
            nodes = obj.G.nodes.coords;
            
            %% The origin of the mesh
            neworigin = min(nodes,[],1);
            
            %% If origin not (0,0), then translate
            nodes = bsxfun(@minus, nodes, neworigin);
            
            obj.G.nodes.coords = nodes;
            
            %% Now shifting fractures
            for i = 1:length(obj.fracnet)
               xy = obj.fracnet{i}.end_points; 
               xy = bsxfun(@minus, xy, neworigin);
               obj.fracnet{i}.end_points = xy;
            end
            
        end
        
        function [obj] = process_intersections(obj)
            
            %% Intersection struct
            obj.intersections.frac_frac = {};
            obj.intersections.global_lists.edges = [];
            obj.intersections.global_lists.cells = [];
            obj.intersections.global_lists.nodes = [];
            
            %% Nothing to do if we have only one fracture
            if(length(obj.fracnet)==1)
                return;
            end
            
            %% Loop over fractures
            nodes = [];
            edges = [];
            cells = [];
            for i = 1:length(obj.fracnet)
               frac = obj.fracnet{i};
               medges = frac.medges;
               sedges = frac.sedges;
               
               for j = 1:size(medges,1)-1
                  na_m = medges(j,1); 
                  nb_m = medges(j,2);
                  
                  na_m_next = medges(j+1,1); 
                  nb_m_next = medges(j+1,1); 
                  
                  na_s = sedges(j,1); 
                  nb_s = sedges(j,2);
                  
                  na_s_next = sedges(j+1,1); 
                  nb_s_next = sedges(j+1,1); 
                  
                  if(nb_m~=na_m_next)
                      %% Appending nodes
                      nodes(end+1,1) = nb_m;
                      nodes(end+1,1) = na_m_next;
                      nodes(end+1,1) = nb_s;
                      nodes(end+1,1) = na_s_next;
                      
                      %% Appending edges
                      edges(end+1,1) = frac.master_edges(j);
                      edges(end+1,1) = frac.master_edges(j+1);
                      edges(end+1,1) = frac.slave_edges(j);
                      edges(end+1,1) = frac.slave_edges(j+1);
                      
                      %% Appending cells
                      cell_m = obj.findElementThatHasEdge(frac.master_edges(j));
                      cell_m_next = obj.findElementThatHasEdge(frac.master_edges(j+1));
                      cell_s = obj.findElementThatHasEdge(frac.slave_edges(j));
                      cell_s_next = obj.findElementThatHasEdge(frac.slave_edges(j+1));
                      
                      cells(end+1,1) = cell_m;
                      cells(end+1,1) = cell_m_next;
                      cells(end+1,1) = cell_s;
                      cells(end+1,1) = cell_s_next;

                  end
               end
               
            end
            obj.intersections.global_lists.nodes = nodes;
            obj.intersections.global_lists.edges = edges;
            obj.intersections.global_lists.cells = cells;
            
            %% Now getting coordinates of intersections
            while(length(nodes)>0)
                reference_node = nodes(1);
                remaining_nodes = nodes(2:end);
               
                rxy = obj.G.nodes.coords(reference_node,:);
                left = bsxfun(@minus, obj.G.nodes.coords(remaining_nodes,:), rxy);
                dist = sqrt(sum(left.^2,2));
                ids = find(dist<=1e-6);
                int.coords = rxy;
                obj.intersections.frac_frac{end+1} = int;
                to_exclude = [1;ids+1];
                nodes(to_exclude) = [];
            end
            
            %% Looping over intersections and getting edges connected to them
            for i = 1:length(obj.intersections.frac_frac)
               intxy = obj.intersections.frac_frac{i}.coords;
               edges = obj.find_edges_connected_to_point(obj.intersections.global_lists.edges,...
                                                     intxy);
               obj.intersections.frac_frac{i}.edges = edges;
            end
                
        end
        
        function conn_edges = find_edges_connected_to_point(obj,edges,intxy)
            conn_edges = [];
            for i = 1:length(edges)
               enodes = obj.utils.edgenodes(edges(i),:);
               axy = obj.G.nodes.coords(enodes(1),:);
               bxy = obj.G.nodes.coords(enodes(2),:);
               
               diff = bsxfun(@minus, [axy;bxy], intxy);
               dist = sqrt(sum(diff.^2,2));
               
               if(any(dist<=1e-8))
                   conn_edges(end+1) = edges(i);
               end
            end
            
        end
        
        
        function obj = build_master_slave_lists(obj)
            
            %% Build master/slave edges
            obj = obj.build_master_slave_edge_lists();
            
            %% Build master/slave nodes
            obj = obj.build_master_slave_node_lists();
            
            %% Build master/slave cells
            obj = obj.build_master_slave_cell_lists();
            
        end
        
%         function obj = process_intersections(obj)
%             
%             for i = 1:length(obj.intersections.frac_frac)
%                 
%                 inti = obj.intersections.frac_frac{i}; 
%                 point = inti.coords;
%                
%                 %% Finding nodes at intersections
%                 intnodes = find(abs(obj.G.nodes.coords(:,1)-point(1))<=1e-05 & ...
%                                abs(obj.G.nodes.coords(:,2)-point(2))<=1e-05); 
%                            
%                 %% Checking consistency of information
%                 assert(length(intnodes)/2 == length(inti.fracs));
%                
%                 %% creating cells and nodes
%                 cells_and_nodes = [];
%                 for k = 1:length(intnodes)
%                   nodek = intnodes(k);
%                   cellsk = find(obj.utils.cellnodes(:,1) == nodek | ...
%                                 obj.utils.cellnodes(:,2) == nodek | ...
%                                 obj.utils.cellnodes(:,3) == nodek);
%                   cells_and_nodes = [cells_and_nodes;...
%                                      cellsk, nodek*(ones(length(cellsk),1))];
%                 end
%                
%                 %% Sorting the cells and nodes clockwise and appending to 
%                 % intersection
%                 [~,ind] = obj.sort_clockwise(cells_and_nodes(:,1));
%                 cells_and_nodes = cells_and_nodes(ind,:);
%                 inti.cells_and_nodes = cells_and_nodes;
%                
%                 %% appending nodes to intersection
%                 inti.nodes = unique(cells_and_nodes(:,2),'stable');
%                
%                 %% Finally, finding edges connected to intersection
%                 edges = [];
%                 for k = 1:length(cells_and_nodes(:,1))
%                    cellk = cells_and_nodes(k,1);
%                    edgesk = obj.utils.celledges(cellk,:);
%                    N = obj.G.faces.neighbors(edgesk,:);
%                    indx = find(N(:,1)==0 | N(:,2)==0);
%                    edges = [edges; edgesk(indx)'];
%                 end
%                
%                 %% appending edges to intersection
%                 inti.edges = edges;
%                
%                 %% Final assignment
%                 obj.intersections.frac_frac{i} = inti;
%                
%                 %% The very useful global lists
%                 obj.intersections.global_lists.cells = ...
%                                       [obj.intersections.global_lists.cells;
%                                        cells_and_nodes(:,1)];
%                 obj.intersections.global_lists.nodes = ...
%                                       [obj.intersections.global_lists.nodes;
%                                        inti.nodes];
%                 obj.intersections.global_lists.edges = ...
%                                       [obj.intersections.global_lists.edges;
%                                        inti.edges];
%                                     
%                 
%             end
%             
%         end
        
        function obj = build_master_slave_cell_lists(obj)
            
           for i = 1:length(obj.fracnet)
               
                master_edges = obj.fracnet{i}.master_edges;
%                 slave_edges = obj.fracnet{i}.slave_edges;
                
                master = [];
                slave = [];
                for j = 1:length(master_edges)
                    master_cell = obj.findElementThatHasEdge(master_edges(j));
%                     slave_cell = obj.findElementThatHasEdge(slave_edges(j));
                    master(end+1,1) = master_cell;
%                     slave(end+1,1) = slave_cell;
                end
                
                obj.fracnet{i}.master_cells = master;
                obj.fracnet{i}.slave_cells = slave;
                
           end
            
        end
        
        function obj = build_master_slave_node_lists(obj)
            
            for i = 1:length(obj.fracnet)
                
                %% First, we sort master nodes
                frac = obj.fracnet{i};
                master = reshape(frac.medges',2*size(frac.medges,1),1);
                master = unique(master,'stable');
                endnodes = master([1,end],:);
                master = setdiff(master,endnodes,'stable');
                
%                 slave = reshape(frac.sedges',2*size(frac.sedges,1),1);
%                 slave = unique(slave,'stable');
%                 endnodes_s = slave([1,end],:);
                
%                 assert( all(endnodes == endnodes_s) );
                
%                 slave = setdiff(slave,endnodes_s,'stable');
                
                obj.fracnet{i}.end_points = [obj.G.nodes.coords(endnodes(1),:);
                                             obj.G.nodes.coords(endnodes(2),:)];
                obj.fracnet{i}.master_nodes = master;
%                 obj.fracnet{i}.slave_nodes = slave;
                
            end
        end
        
        function obj = build_master_slave_edge_lists(obj)
            
            for i = 1:length(obj.fracnet)
                frac = obj.fracnet{i}; 
                medges = [];
                sedges = [];
                for j = 1:size(frac.medges,1)
                    na = frac.medges(j,1); 
                    nb = frac.medges(j,2);
                    me = find(obj.utils.edgenodes(:,1)==na &...
                       obj.utils.edgenodes(:,2)==nb |...
                       obj.utils.edgenodes(:,1)==nb &...
                       obj.utils.edgenodes(:,2)==na);
                    assert(~isempty(me));
                    
%                     na = frac.sedges(j,1); 
%                     nb = frac.sedges(j,2);
%                     se = find(obj.utils.edgenodes(:,1)==na &...
%                        obj.utils.edgenodes(:,2)==nb |...
%                        obj.utils.edgenodes(:,1)==nb &...
%                        obj.utils.edgenodes(:,2)==na);
%                     assert(~isempty(se));
                    
                    medges = [medges; me];
%                     sedges = [sedges; se];
                end
%                 assert(length(medges)==length(sedges));
%                 assert(isempty(find(ismember(medges,sedges))));
                obj.fracnet{i}.master_edges = medges;
                obj.fracnet{i}.slave_edges = sedges;
               
            end
            
        end
        
        function [ele,nodes,origin] = read_mesh(obj, filemesh)
    
            A = dlmread(filemesh);
            nnodes = A(1,1);
            nodes = A(2:nnodes+1,1:2);
            ele = A(nnodes+3:end,:);
            
            %% Porepy numbering starts with 0
            if(min(min(ele))==0)
                ele = ele + 1;
            end
            
            %% The origin of the mesh
            origin = min(nodes,[],1);
            
            %% If origin not (0,0), then translate
            nodes = bsxfun(@minus, nodes, origin);

        end
        
        function [ele,nodes] = renumber2D(obj, ele, nodes)

            for i = 1:length(ele)
               nA = nodes(ele(i,1),:)';
               nB = nodes(ele(i,2),:)';
               nC = nodes(ele(i,3),:)';
               A = [nA,nB,nC;1,1,1];
               area = 0.5*det(A);
               if(area<0)
                   ele(i,:) = ele(i,end:-1:1);
               end
            end

        end
       
        function [fracnet] = read_fracs_tortuous(obj, filefrac)
            
            filefrac_medges = [filefrac, '_medges.txt'];
            filefrac_sedges = [filefrac, '_sedges.txt'];
            
            %% Initializing fracture cell array
            fracnet = {};
            
            %% Initializing fracture cell array
            fracnet = {};

            Am = dlmread(filefrac_medges);
%             As = dlmread(filefrac_sedges);
            i = 1;
            while i <= size(Am,1)
                nedges_m = Am(i);
%                 nedges_s = As(i);
                
%                 assert(nedges_m==nedges_s);
                
                medges = Am([i+1:i+nedges_m],1:2);
%                 sedges = As([i+1:i+nedges_s],1:2);
                frac.medges = medges;
%                 frac.sedges = sedges;
                fracnet{end+1} = frac;
                
                i = i + nedges_m + 1;
            end
            
        end
        
        function [mfracs] = get_mother_fractures(obj,fracedges)

            mfracs = zeros(length(fracedges),1);
            for i = 1:length(fracedges)
               for j = 1:length(obj.fracnet)
                   
                    %% The three points
                    xA = obj.fracnet{j}.end_points(1,:);
                    xB = obj.fracnet{j}.end_points(2,:);
                    xT = obj.G.faces.centroids(fracedges(i),:);
                    
                    if(is_point_in_line(xA, xB, xT))
                       mfracs(i) = j; 
                    end
                           
               end
               
            end
            
        end
        
        function [id] = findElementThatHasEdge(obj,edge)
            
            elementedges = reshape(obj.G.cells.faces',3,length(obj.G.cells.faces)/3)';
            id = find(elementedges(:,1) == edge | ...
                      elementedges(:,2) == edge | ...
                      elementedges(:,3) == edge);
                  
        end
        
        function [cells,ind] = sort_clockwise(obj,cells)
            
            %% Given a bunch of cells, sort them clockwise
            x = obj.G.cells.centroids(cells,1);
            y = obj.G.cells.centroids(cells,2);
            cx = mean(x);
            cy = mean(y);

            a = atan2(y - cy, x - cx);

            [~, ind] = sort(a);
            cells = cells(ind);
            
        end
        
        function [mors] = master_or_slave(obj,fracedges,mfracs)
            
            mors = zeros(length(fracedges),1);
            for i = 1:length(fracedges)
               edgei = fracedges(i);
               elid = findElementThatHasEdge(obj,edgei);
               e = obj.fracnet{mfracs(i)}.end_points;
               eA = e(1,:);
               eB = e(2,:);
               nA = obj.utils.edgenodes(edgei,1);
               nB = obj.utils.edgenodes(edgei,2);
               pA = obj.G.nodes.coords(nA,:);
               pB = obj.G.nodes.coords(nB,:);
               d = dot(eB-eA,pB-pA);
               if(d<0)
                   aux = pB;
                   pB = pA;
                   pA = aux;
               end
               cE = obj.G.cells.centroids(elid,:);

               u = pB - pA;
               v = cE - pA;

               c = cross([u,0],[v,0]);

               if( c(3) > 0 )
                   mors(i)=1;
               else
                   mors(i)=-1;
               end

            end
            
        end
        
    end
        
end

function r = is_point_in_line(xA, xB, xT)

    [x1,y1] = deal(xA(1),xA(2));
    [x2,y2] = deal(xB(1),xB(2));
    [xt,yt] =  deal(xT(1),xT(2));
    
    u = xB-xA;
    v = xT-xA;
    u(3) = 0;
    v(3) = 0;
    
    r1 = (abs(norm(cross(u,v)))<=1e-04);
    
    r2 = (xt >= min(x1,x2)) &...
         (xt <= max(x1,x2)) &...
         (yt >= min(y1,y2)) &...
         (yt <= max(y1,y2));
     
    r = r1*r2;
       
end


