function [p, cb] = plot_fracture_data(mesh, data, varargin)

    opt = struct('line_thickness', 3);
    opt = merge_options(opt, varargin{:});
    
    data = reshape(data,length(data),1);
    p = {};
    counter = 1;
    for i = 1:length(mesh.fracnet)
        medges = mesh.fracnet{i}.master_edges;
        exy = mesh.G.faces.centroids(medges,:);
        x = exy(:,1);
        y = exy(:,2);
        z = zeros(size(x));
        d = data(counter : counter + length(medges) - 1,1);
        
        p{end+1} = surface([x';x'],[y';y'],[z';z'],[d';d'],...
                'facecol','no',...
                'edgecol','interp',...
                'linew',opt.line_thickness);
        hold on;

        counter = counter + length(medges);
    end
    cb = colorbar;    
     
end