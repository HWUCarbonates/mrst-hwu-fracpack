function [p] = plot_mesh(mesh,varargin)

    opt = struct('n', false, 'e', false, 'c', false, 'f', true);
    opt = merge_options(opt, varargin{:});
            
    %% Plotting grid
    p = plotGrid(mesh.G);
            
    %% Making plot pretty
    fig = gcf;
    fig.Color = 'w';
    set(gca,'FontSize',16)
    p.FaceColor = 'w';
    p.EdgeAlpha = 0.2;

            
    %% Plotting cell ids
    if(opt.c)
        colour = rand(1,3);
        for i = 1:mesh.G.cells.num
            text(mesh.G.cells.centroids(i,1),...
                 mesh.G.cells.centroids(i,2),...
                 num2str(i),...
                 'FontSize',12,...
                 'Color',colour)
        end
    end
            
    %% Plotting edge ids
    if(opt.e)
        colour = rand(1,3);
        for i = 1:mesh.G.faces.num
            text(mesh.G.faces.centroids(i,1)+rand,...
                 mesh.G.faces.centroids(i,2)+rand,...
                 num2str(i),...
                 'FontSize',12,...
                 'Color',colour)
        end
    end

    %% Plotting node ids
    if(opt.n)
        colour = rand(1,3);
        for i = 1:mesh.G.nodes.num
            text(mesh.G.nodes.coords(i,1)+rand,...
                 mesh.G.nodes.coords(i,2)+rand,...
                 num2str(i),...
                 'FontSize',12,...
                 'Color',colour)
        end
    end
            
    %% Plotting fractures
    if(opt.f)
        for i = 1:length(mesh.fracnet)
           fraci = mesh.fracnet{i};
           if(isfield(fraci,'end_points'))
               endpoints = fraci.end_points;
               hold on;
               plot(endpoints(:,1),endpoints(:,2),...
                    'LineWidth',1.5,'Color','k');
               hold on;
           else
               warning('No end points found');
        end
    end
            
end