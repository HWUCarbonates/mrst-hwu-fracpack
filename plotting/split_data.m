function [datam, dataf] = split_data(solver,data,varargin)
    
    if(solver.fracture_only)
        datam = [];
        dataf = data;
    else
        datam = data(1:solver.mesh.G.cells.num);
        dataf = data(solver.mesh.G.cells.num+1:end);
    end

end