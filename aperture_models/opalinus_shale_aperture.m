function [e] = opalinus_shale_aperture(cpress)

    sigman = cpress/mega;
    
    alpha = 0.45;
    beta = 0.75;
    bm = 0.015;
    
    db = bm*(1-exp(-alpha*sigman.^beta));
    
    e = (bm-db)*milli;
    
end

