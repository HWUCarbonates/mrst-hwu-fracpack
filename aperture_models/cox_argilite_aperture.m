function [e] = cox_argilite_aperture(cpress, varargin)

    opt = struct('core_number',1);
    opt = merge_options(opt, varargin{:});   
    
    
    switch opt.core_number
        
        case 1
            bm = 2.3;
        case 2
            bm = 1.4;
        case 3
            bm = 1.3;
        case 4
            bm = 0.6;
        case 5
            bm = 0.45;
    end
    
    
    sigman = cpress/mega;
    
    alpha = 0.3;
    beta = 0.5;

    db = bm*(1-exp(-alpha*sigman.^beta));
    
    e = (bm-db)*milli;
    
end

