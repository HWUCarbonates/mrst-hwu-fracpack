%% Attention: This example does not work anymore after we renamed
% UniversalFracturedMesh to FracturedMesh.

% clear;
% close all;
% clc;
% 
% load('example_mesh.mat');
% 
% en_data = read_abaqus_element_node_data( mesh, '.\', 'example');
% n_data = read_abaqus_node_data( mesh, '.\', 'example');
% 
% figure()
% p = plot_fracture_data(mesh,en_data.values.cpress/mega,'line_thickness',3);
% title('contact pressure [MPa]');
% 
% figure()
% p = plotNodeData(mesh.G,n_data.values.s(:,2));
% p.EdgeAlpha = 0.1;
% title('displacement x [m]');
