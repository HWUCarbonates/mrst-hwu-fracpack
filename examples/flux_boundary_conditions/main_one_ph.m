clear;
clc;
close all;

mrstModule add ad-core;

%% Creating mesh
if isunix
    path='./mesh_files/';
else
    path = '.\mesh_files\';
end

mesh = FracturedMesh(path,'ab_2','abaqus');

mesh = mesh.build_master_slave_lists();
mesh = mesh.process_intersections();

%% solver
solver = SinglePhaseFlowSolver(mesh,'fracture_only',false);

%% Setting BCs
% dp = 5000*psia;
% value = dp;
% side = 'ymin';
% medium = 'm';
% solver = solver.set_pressure_bc(value, side, medium);
q = 1e-08;
value = q;
side = 'ymin';
medium = 'm';
solver = solver.set_flux_bc(value, side, medium);

value = 0*psia;
side = 'ymax';
medium = 'm';
solver = solver.set_pressure_bc(value, side, medium);  

%% Setting fluid
solver = solver.set_fluid('muw',1,'mun',1,'nw',1,'nn',1);
                      
%% Setting matrix rock
k = 1*milli*darcy;
phi = 0.1;
matrix_rock = makeRock(solver.mesh.G, k, phi);
solver = solver.set_matrix_rock(matrix_rock);

%% Updating fracture properties
ap = 1*milli;
kf = ap.^2/12;
% kf = k;
phif = 1;
fprops = constant_fracture_properties( solver.mesh, ap,kf,phif);
solver = solver.update_fracture_properties(fprops);

%% Computing transmissibility
solver = solver.compute_transmissibility();

%% Initializing state
state = initialize_state(solver, 1, 0*psia);

fig = figure('Position',[100,100,1200,500]);

%% Solving Pressure
[state] = solver.solve_pressure(state);
[pnm, pnf] = split_data(solver,state.pn);

%% Plotting fracture field
figure(fig)
colormap(jet);
if(~isempty(pnm))
    p = plotCellData(solver.mesh.G,pnm);
    p.EdgeAlpha = 0.1;
else
    p = plot_mesh(solver.mesh);
    p.EdgeAlpha = 0.1;
end
p = plot_fracture_data(solver.mesh,pnf);
colorbar;
axis equal;
set(gca,'FontSize',16);
drawnow;
qw = state.bc_fluxes.val;
qin = sum(qw(qw > 0))
qout = sum(qw(qw < 0))

dPdarcy = qin/k

% 
% direction = 'y';
% keff = effective_permeability(solver, state.bc_fluxes, direction, dp);
% keff/(milli*darcy)




