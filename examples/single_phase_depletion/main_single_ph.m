clear;
clc;
close all;

mrstModule add ad-core;

%% Creating mesh
if isunix
    path='./mesh_files/';
else
    path = '.\mesh_files\';
end

mesh = FracturedMesh(path,'pp','porepy');

mesh = mesh.build_master_slave_lists();
mesh = mesh.process_intersections();

%% solver
solver = TwoPhaseFlowSolver(mesh,'fracture_only',false);

%% Setting BCs
value = 0*psia;
side = 'ymax';
medium = 'm';
solver = solver.set_pressure_bc(value, side, medium, 'sat', [1,0]);  

%% Setting heterogeneous rel. perms
nm = solver.mesh.G.cells.num;
nf = solver.virtual_cells.num;
matrix_cells = 1:nm;
fracture_cells = nm+1:nm+nf;

solver = solver.set_fluid('muw',1,'mun',1,'cw',1e-8,'cn',0,...
                          'rhow',1,'rhon',1,'pref',0,...
                          'nw',1,...
                          'nn',1);
                      
%% Setting matrix rock
k = 10*milli*darcy;
phi = 0.1;
matrix_rock = makeRock(solver.mesh.G, k, phi);
solver = solver.set_matrix_rock(matrix_rock);

%% Updating fracture properties
ap = 0.1*milli;
kf = ap.^2/12;
phif = 1;
fprops = constant_fracture_properties( solver.mesh, ap,kf,phif);
solver = solver.update_fracture_properties(fprops);

%% Computing transmissibility
solver = solver.compute_transmissibility();

%% Initializing state
state = initialize_state(solver, 1, 7*psia);

fig = figure('Position',[100,100,1200,500]);

%% Advancing fields
dt = 10*day;
for i = 1:100
    [state] = solver.solve_timestep(state,dt,'tol',1e-15);
    [swm, swf] = split_data(solver,state.sw);
    [pnm, pnf] = split_data(solver,state.pn);
    
    %% Plotting fracture field
    figure(fig)
    subplot(1,2,1)
    colormap(jet);
    if(~isempty(swm))
        p = plotCellData(solver.mesh.G,swm);
        p.EdgeAlpha = 0.1;
    else
        p = plot_mesh(solver.mesh);
        p.EdgeAlpha = 0.1;
    end
    p = plot_fracture_data(solver.mesh,swf,'line_thickness',3);
    colorbar;
    set(gca,'FontSize',16);
    axis equal;
    drawnow;
    
    subplot(1,2,2)
    colormap(jet);
    if(~isempty(pnm))
        p = plotCellData(solver.mesh.G,pnm/psia);
        p.EdgeAlpha = 0.1;
    else
        p = plot_mesh(solver.mesh);
        p.EdgeAlpha = 0.1;
    end
    p = plot_fracture_data(solver.mesh,pnf/psia);
    colorbar;
    axis equal;
    set(gca,'FontSize',16);
    drawnow;
    
    qw = double(state.bc_fluxes{1});
    qo = double(state.bc_fluxes{2});
    qwin = sum(qw(qw > 0))
    qwout = sum(qw(qw < 0))
    qoin = sum(qo(qo > 0))
    qoout = sum(qo(qo < 0))
    
end


