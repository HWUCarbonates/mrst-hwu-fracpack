clear;
clc;
close all;

mrstModule add ad-core;

%% creating mesh
if isunix
    path='./mesh_files/';
else
    path = '.\mesh_files\';
end

mesh = FracturedMesh(path,'pp','porepy');

mesh = mesh.build_master_slave_lists();
mesh = mesh.process_intersections();

% plot_mesh(mesh);

%% solver
mech_solver = MechanicsSolver(mesh);

%% set elastic parameters
E = 50e+09;
nu = 0.2;
mech_solver = mech_solver.set_elastic_parameters(E,nu);

%% setting boundary conditions
mech_solver = mech_solver.set_no_rotation_bc();

value = 20*mega*Pascal;
direction = 'x';
mech_solver = mech_solver.set_load_bc(value,direction);

value = 10*mega*Pascal;
direction = 'y';
mech_solver = mech_solver.set_load_bc(value,direction);

%% get contact pressure
[cpress] = mech_solver.get_contact_pressure();

%% get barton bandis aperture
[E,e] = barton_bandis_aperture(cpress,'JRC',2,'JCS',50);

%% calculate permeability
kf = e.^2/12;

figure(1);
plot_mesh(mesh);
hold on;
plot_fracture_data(mesh,cpress/mega);
colormap jet;

figure(2);
plot_mesh(mesh);
hold on;
plot_fracture_data(mesh,e/milli);
colormap jet;

figure(3);
plot_mesh(mesh);
hold on;
plot_fracture_data(mesh,kf/(darcy));
colormap jet;



%% Node data
% colormap jet;
% p = plotNodeData(mesh.G,cpress/mega);
% p.EdgeAlpha = 0.1;
% colorbar;

