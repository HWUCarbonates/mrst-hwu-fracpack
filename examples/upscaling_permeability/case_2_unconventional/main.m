clear;
clc;
close all;

mrstModule add ad-core;

%% Creating mesh
if isunix
    path='./mesh_files/';
else
    path = '.\mesh_files\';
end
% ab_2 mesh differs from ab_1 mesh in just one more fracture connection 
% making the DFN a percolating one.
% This results in an upscaled permability 6 orders of magnitude higher
prefix = 'ab_2';

mesh = FracturedMesh(path,prefix,'abaqus');

mesh = mesh.build_master_slave_lists();
mesh = mesh.process_intersections();

% save([path,prefix,'_mesh.mat'],'mesh')
% load([path,prefix,'_mesh.mat']);

%% solver
solver = SinglePhaseFlowSolver(mesh,'fracture_only',false);

%% Setting BCs
dp = 10*psia;
value = dp;
side = 'ymin';
medium = 'f'; % set bc in the fracture network rather than on the domain
solver = solver.set_pressure_bc(value, side, medium, 'tol', 5.1);

value = 0*psia;
side = 'ymax';
medium = 'f'; %  set bc in the fracture network rather than on the domain
solver = solver.set_pressure_bc(value, side, medium, 'tol', 5.1);  

%% Setting fluid
solver = solver.set_fluid('muw',1,'mun',1,'nw',1,'nn',1,'krwmax',1,'krnmax',1);
                      
%% Setting matrix rock
km = 1*nano*darcy;
phi = 0.05;
matrix_rock = makeRock(solver.mesh.G, km, phi);
solver = solver.set_matrix_rock(matrix_rock);

%% Setting buffer
left_cells = find(solver.mesh.G.cells.centroids(:,1)<5);
bottom_cells = find(solver.mesh.G.cells.centroids(:,2)<5);
right_cells = find(solver.mesh.G.cells.centroids(:,1)>60-5);
top_cells = find(solver.mesh.G.cells.centroids(:,2)>60-5);
matrix_rock.perm(bottom_cells,:) = 10000*darcy;
matrix_rock.perm(top_cells,:) = 10000*darcy;
matrix_rock.perm(left_cells,:) = km/100000;
matrix_rock.perm(right_cells,:) = km/100000;

%% Updating fracture properties
ap = 0.4*milli;
kf = ap.^2/12;
phif = 1;
fprops = constant_fracture_properties( solver.mesh, ap,kf,phif);
solver = solver.update_fracture_properties(fprops);

%% Computing transmissibility
solver = solver.compute_transmissibility();

%% Initializing state
state = initialize_state(solver, 1, 0*psia);

fig = figure('Position',[100,100,1200,500]);

%% Solving Pressure
[state] = solver.solve_pressure(state);
[pnm, pnf] = split_data(solver,state.pn);

%% Plotting fracture field
figure(fig)
colormap(jet);
if(~isempty(pnm))
    p = plotCellData(solver.mesh.G,pnm/psia);
    p.EdgeAlpha = 0.1;
else
    p = plot_mesh(solver.mesh);
    p.EdgeAlpha = 0.1;
end
p = plot_fracture_data(solver.mesh,pnf/psia);
colorbar;
axis equal;
set(gca,'FontSize',16);
drawnow;

direction = 'y';
keff = effective_permeability(solver, state.bc_fluxes, direction, dp);
keff/(milli*darcy)




