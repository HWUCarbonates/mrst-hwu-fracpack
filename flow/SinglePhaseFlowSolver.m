classdef SinglePhaseFlowSolver
    %MESOFLOWSOLVER Mesoscale Flow solver
    
    properties
        mesh
        fracture_only
        virtual_cells
        conns
        conn_trans
        operators
        half_trans
        bc
        T_all
        matrix_rock
        ad_backend
        fluid_info
        fluid
    end
    
    methods
        
        function obj = SinglePhaseFlowSolver(mesh,varargin)

            opt = struct('fracture_only', false);
            opt = merge_options(opt, varargin{:});                         
            
            obj.fracture_only = opt.fracture_only;
            obj.mesh = mesh;
            
            %% Creating virtual cells
            obj = obj.create_virtual_cells();
            obj = obj.create_connection_list();
            obj = obj.create_operators();
            
            obj.conn_trans.matrix = [];
            obj.conn_trans.virtual = [];
            obj.conn_trans.virtual_int = [];
            obj.half_trans.fracfrac = zeros(obj.virtual_cells.num,1);
            
            if(~obj.fracture_only)
                obj.half_trans.fracmat = zeros(obj.virtual_cells.num,1);
                obj.half_trans.master = zeros(obj.virtual_cells.num,1);
                obj.half_trans.slave = zeros(obj.virtual_cells.num,1);
            end
            
            obj.bc.cells = [];
            obj.bc.faces = [];
            obj.bc.values = [];
            obj.bc.medium = [];
            obj.bc.sat = [];
			obj.bc.type = [];
            
            obj.T_all = [];
            
            obj.ad_backend = AutoDiffBackend();
            
        end
        
        function [obj] = set_fluid(obj,varargin)
            
            obj.fluid_info = struct('muw',1,'mun',1,'cw',1e-20,'cn',1e-20,...
                          'rhow',1,'rhon',1,'pref',0,'nw',1,'nn',1,...
                          'krwm', 1.0,'krnm', 1.0);
            obj.fluid_info = merge_options(obj.fluid_info, varargin{:}); 
            
            obj.fluid.rhow = @(p)obj.fluid_info.rhow * ...
                                exp(obj.fluid_info.cw * ...
                                (p - obj.fluid_info.pref));
                            
            obj.fluid.rhon = @(p)obj.fluid_info.rhon * ...
                                exp(obj.fluid_info.cn * ...
                                (p - obj.fluid_info.pref));

            if(length(obj.fluid_info.nw)==1)
                nm = obj.mesh.G.cells.num;
                nf = obj.virtual_cells.num;
                if(obj.fracture_only)
                    nc = nf;
                else
                    nc = nm+nf;
                end
                obj.fluid_info.nw = obj.fluid_info.nw*ones(nc,1);
                obj.fluid_info.nn = obj.fluid_info.nn*ones(nc,1);
            end
            
            if(length(obj.fluid_info.krwm)==1)
                nm = obj.mesh.G.cells.num;
                nf = obj.virtual_cells.num;
                if(obj.fracture_only)
                    nc = nf;
                else
                    nc = nm+nf;
                end
                obj.fluid_info.krwm = obj.fluid_info.krwm*ones(nc,1);
                obj.fluid_info.krnm = obj.fluid_info.krnm*ones(nc,1);
            end
            
            obj.fluid.krw =  @(sw,cells)obj.fluid_info.krwm(cells).*...
                                       sw(cells).^obj.fluid_info.nw(cells);
            obj.fluid.krn =  @(sw,cells)obj.fluid_info.krnm(cells).*...
                                    (1-sw(cells)).^obj.fluid_info.nn(cells);
            
        end
        
		function [obj] = set_pressure_bc(obj, value, side, medium, varargin)
            
            opt = struct('tol', 1*meter, 'sat', [1,0]);
            opt = merge_options(opt, varargin{:});
            
            if(obj.fracture_only && strcmp(medium,'m'))
               medium = 'f';
               warning(['You are injecting in the matrix, but flow happens ',...
                     'only in the fractures. Setting inject_in_fractures = true']);
            end
            
            if(strcmp(medium,'m'))
                [cells,faces] = get_boundary_cells( obj, side, 'm');
                obj.bc.medium = [obj.bc.medium; repmat('m',length(cells),1)];
            elseif(strcmp(medium,'f'))
                [cells,faces] = get_boundary_cells( obj, side, 'f',...
                                            'tol',opt.tol);
                obj.bc.medium = [obj.bc.medium; repmat('f',length(cells),1)];
            else
                error('Medium should be either "m" or "f"');
            end
            obj.bc.faces = [obj.bc.faces; faces];
            obj.bc.cells = [obj.bc.cells; cells];
            obj.bc.values = [obj.bc.values; value*ones(length(cells),1)];
            obj.bc.sat = [obj.bc.sat;...
                                 repmat(opt.sat,length(cells),1)];
            obj.bc.type = [obj.bc.type; repmat('p',length(cells),1)];
        end
        
        function [obj] = set_flux_bc(obj, value, side, medium, varargin)
            
            opt = struct('tol', 1*meter, 'sat', [1,0]);
            opt = merge_options(opt, varargin{:});
            
            if(obj.fracture_only && strcmp(medium,'m'))
               medium = 'f';
               warning(['You are injecting in the matrix, but flow happens ',...
                     'only in the fractures. Setting inject_in_fractures = true']);
            end
            
            if(strcmp(medium,'m'))
                [cells,faces] = get_boundary_cells( obj, side, 'm');
                obj.bc.medium = [obj.bc.medium; repmat('m',length(cells),1)];
            elseif(strcmp(medium,'f'))
                [cells,faces] = get_boundary_cells( obj, side, 'f',...
                                            'tol',opt.tol);
                obj.bc.medium = [obj.bc.medium; repmat('f',length(cells),1)];
            else
                error('Medium should be either "m" or "f"');
            end
            obj.bc.faces = [obj.bc.faces; faces];
            obj.bc.cells = [obj.bc.cells; cells];
            obj.bc.values = [obj.bc.values; (value/length(cells))*ones(length(cells),1)];
            obj.bc.sat = [obj.bc.sat;...
                                 repmat(opt.sat,length(cells),1)];
            obj.bc.type = [obj.bc.type; repmat('q',length(cells),1)];
        end
        
        function obj = compute_transmissibility(obj)
            
            
            %% Frac-frac connections
            obj.conn_trans.virtual = zeros(length(obj.conns.virtual),1);
            for i = 1:length(obj.conns.virtual)
                id1 = obj.conns.virtual(i,1);
                id2 = obj.conns.virtual(i,2);
                
                % id1 is always a virtual cell. This, we know for sure
                [~,locid1] = ismember(id1,obj.virtual_cells.global_ids);
                
                % is id2 a virtual cell ? 
                isvirtual = id2 > obj.mesh.G.cells.num;
                
                if(isvirtual)
                    t1 = obj.half_trans.fracfrac(locid1);
                    
                    % id2 is a virtual cell
                    [~,locid2] = ismember(id2,obj.virtual_cells.global_ids);
                    t2 = obj.half_trans.fracfrac(locid2);
                else
                    t1 = obj.half_trans.fracmat(locid1);
                    % id2 is either a master or a slave cell
                    if( id2 == obj.virtual_cells.master_slave_info.mcells(locid1) )
                        % id2 is master
                        t2 = obj.half_trans.master(locid1);
                    else
                        % id2 is slave
                        t2 = obj.half_trans.slave(locid1);
                    end
                end
                
                obj.conn_trans.virtual(i) = (t1*t2)/(t1+t2);
                
            end
            
            %% intersections
            obj.conn_trans.virtual_int = zeros(length(obj.conns.virtual_int),1);
            nints = length(obj.mesh.intersections.frac_frac);
            
            for i = 1:nints
                intindx = find( obj.conns.vconns_int_id == i );
                try
                    restricted_conns = obj.conns.virtual_int(intindx,:);
                catch
                    disp('stop')
                end
                cells = unique(restricted_conns);
                ids = ismember(obj.virtual_cells.global_ids,cells);
                tsum = sum(obj.half_trans.fracfrac(ids));
                
                for j = 1:size(restricted_conns,1)
                   id1 = restricted_conns(j,1);
                   id2 = restricted_conns(j,2);
                   id1 = find(obj.virtual_cells.global_ids == id1);
                   id2 = find(obj.virtual_cells.global_ids == id2);
                   t1 = obj.half_trans.fracfrac(id1);
                   t2 = obj.half_trans.fracfrac(id2);
                   obj.conn_trans.virtual_int(intindx(j)) = t1*t2/tsum;
                end
            end
            
        end
        
        function obj = set_matrix_rock(obj, matrix_rock)
            
            %% Assertion
            if(obj.fracture_only)
                warning('Why are you setting a rock if there is no matrix flow ?');
            end
            
            obj.matrix_rock = matrix_rock;
            
            %% Setting matrix matrix transmissibility
            s = setupOperatorsTPFA(obj.mesh.G,...
                                   obj.matrix_rock);
            mtrans = s.T;
            obj.conn_trans.matrix = mtrans;
            
            %% Setting half-face transmissibility between fracture and 
            % matrix
            obj.half_trans.slave = s.T_all(obj.virtual_cells.master_slave_info.sedges);
            obj.half_trans.master = s.T_all(obj.virtual_cells.master_slave_info.medges);
           
            %% setting matrix transmissibilities including boundary
            obj.T_all = s.T_all;
            
        end
        
        function obj = update_fracture_properties(obj, fprops)
            % This function simply updates the half transmissibilities in 
            % obj.half_trans.fracfrac and obj.half_trans.fracmat and 
            % fracture_pore_volume. We receive a struc fprops with the
            % following fields: fprops.master_edges, fprops.values.aperture,
            % fprops.values.permeability and fprops.values.porosity. Note that
            % we identify the information living in the virtual cells by the
            % master_edges ids.

            %% Mapping information
            [~,idx] = ismember(fprops.master_edges,...
                               obj.virtual_cells.master_slave_info.medges);

            midx = obj.virtual_cells.master_slave_info.medges(idx);

            %% Area between fracture and matrix cells
            areas = obj.mesh.G.faces.areas(midx);

            %% Convenience variables
            k = fprops.values.permeability;
            a = fprops.values.aperture;

            %% Setting frac-frac half trans
            obj.half_trans.fracfrac(idx) = (a.*k)./(areas/2);

            %% Setting frac-mat half trans
            obj.half_trans.fracmat(idx) = (2*areas.*k)./a;

        end
            
        function obj = create_operators(obj)
            
            %% Neighbors
            if(obj.fracture_only)
                N = [];
                nc = 0;
                nf = 0;
            else
                N = obj.conns.matrix;
                nc = obj.mesh.G.cells.num;
                nf = size(N,1);
            end
            
            N = [N; obj.conns.virtual]; 
            nc = nc + obj.virtual_cells.num;
            nf = nf + size(obj.conns.virtual,1);
            
            %% Adding intersections
            N = [N; obj.conns.virtual_int]; 
            nf = nf + size(obj.conns.virtual_int,1);
            
            %% This is necessary to construct the D matrix
            if(obj.fracture_only)
               N = N - obj.mesh.G.cells.num;
            end
            
            %% Define discrete operators
            obj.operators.nc = nc;
            obj.operators.nf = nf;
            obj.operators.N = N;
            obj.operators.D = sparse([(1:nf)'; (1:nf)'],N,...
                                       ones(nf,1)*[-1 1],nf,nc);
            obj.operators.grad  = @(x)  obj.operators.D *x;
            obj.operators.div   = @(x) -obj.operators.D'*x;
            obj.operators.avg   = @(x) 0.5 * (x(obj.operators.N(:,1)) +...
                                              x(obj.operators.N(:,2)));
            obj.operators.upw   = @(flag, x) flag.*x(obj.operators.N(:, 1)) +...
                                            ~flag.*x(obj.operators.N(:, 2));
            
        end
        
        function obj = create_virtual_cells(obj)
            
            %% Initializing arrays
            obj.virtual_cells.num = 0;
            obj.virtual_cells.global_ids = [];
            obj.virtual_cells.fracs = [];
            obj.virtual_cells.master_slave_info.medges = [];
            obj.virtual_cells.master_slave_info.sedges = [];
            obj.virtual_cells.master_slave_info.mcells = [];
            obj.virtual_cells.master_slave_info.scells = [];
            
            %% Loop over fracture network - setting ids and mesh information
            count = obj.mesh.G.cells.num + 1;
            for i = 1:length(obj.mesh.fracnet)
                frac = obj.mesh.fracnet{i};
                ncells = length(frac.master_edges);
                ids = ( count : count + ncells - 1 )';
                medges = frac.master_edges;
                sedges = frac.slave_edges;
                mcells = frac.master_cells;
                scells = frac.slave_cells;

                %% Global ids that start after the last cell id of the G structure
                obj.virtual_cells.global_ids = [obj.virtual_cells.global_ids; ids];
                
                %% The id of the fracture of each virtual cell
                obj.virtual_cells.fracs = [obj.virtual_cells.fracs; i*ones(ncells,1)];
                
                %% master and slave information. Useful for mapping 
                % between flow and mechanics
                obj.virtual_cells.master_slave_info.medges = ...
                [obj.virtual_cells.master_slave_info.medges; medges];
                obj.virtual_cells.master_slave_info.sedges = ...
                [obj.virtual_cells.master_slave_info.sedges; sedges];
                obj.virtual_cells.master_slave_info.mcells = ...
                [obj.virtual_cells.master_slave_info.mcells; mcells];
                obj.virtual_cells.master_slave_info.scells = ...
                [obj.virtual_cells.master_slave_info.scells; scells];

                %% incrementing the number of virtual cells
                count = count + ncells;
            end
            
            %% Setting number of virtual cells
            obj.virtual_cells.num = length(obj.virtual_cells.global_ids);
           
        end
        
        function obj = create_connection_list(obj)
            
            %% Matrix-matrix connections
            mconns = double(obj.mesh.G.faces.neighbors); 
            intInx = all(mconns ~= 0, 2);
            mconns = mconns(intInx, :);
            
            %% Fracture-matrix connections (virtual cell <-> master cell and
            % virtual_cell <-> slave cell
            vconns_fm = [];
            vconns_fm = [vconns_fm; obj.virtual_cells.global_ids,...
                                    obj.virtual_cells.master_slave_info.mcells];
            vconns_fm = [vconns_fm; obj.virtual_cells.global_ids,...
                                    obj.virtual_cells.master_slave_info.scells];
            
            %% Fracture-fracture connections
            vconns_ff = [];
            for i = 1:length(obj.mesh.fracnet)
               indfrac = find(obj.virtual_cells.fracs == i); 
               global_ids = obj.virtual_cells.global_ids(indfrac);
               for j = 1:length(global_ids)-1
                   vconns_ff = [vconns_ff; global_ids(j),global_ids(j+1)];
               end
            end
            
            %% Adding intersections to the connection list
            vconns_int = [];
            vconns_int_id = [];
            for i = 1:length(obj.mesh.intersections.frac_frac)
                int = obj.mesh.intersections.frac_frac{i};
                [~,ids] = ismember(int.edges,...
                                   obj.virtual_cells.master_slave_info.medges);
                vcells = obj.virtual_cells.global_ids(ids(ids~=0));
                vconns_int = [vconns_int; nchoosek(vcells,2)];
                vconns_int_id = [vconns_int_id; i*ones(length(nchoosek(vcells,2)),1)];
            end
            
            %% Important treatment to avoid counting cell connected to intersection twice
            if(~isempty(vconns_int))
                vconns_int_reverse = vconns_int(:,2:-1:1);
                [~,exists] = intersect(vconns_ff,vconns_int,'rows');
                [~,exists_reverse] = intersect(vconns_ff,vconns_int_reverse,'rows');
                vconns_ff([exists;exists_reverse],:) = [];
            end
            
            %% Concatenation of virtual connections
            if(obj.fracture_only)
                vconns = vconns_ff;
            else
                vconns = [vconns_fm; vconns_ff];
            end
            
            %% Final assignment
            if(~obj.fracture_only)
                obj.conns.matrix = mconns;
            else
                obj.conns.matrix = [];
            end
            obj.conns.virtual = vconns;
            obj.conns.virtual_int = vconns_int;
            obj.conns.vconns_int_id = vconns_int_id;
            
        end
        
        function [state] = solve_pressure(obj,state)
            
            %% Transmissibility, pore volume and number of cells
            if(obj.fracture_only)
                T = [obj.conn_trans.virtual;
                     obj.conn_trans.virtual_int];
                nc = obj.virtual_cells.num;
            else
                T = [obj.conn_trans.matrix;
                     obj.conn_trans.virtual;
                     obj.conn_trans.virtual_int];
                nc = obj.mesh.G.cells.num +...
                     obj.virtual_cells.num;
            end

            %% Indices
            pIx = 1:nc;

            %% previous pressure and saturation
            pn0 = state.pn;
            sw0 = state.sw;
            
            %% Initializing AD variables
            [pn, sw] = initVariablesADI(pn0,sw0);
            
            nit = 0;
            resNorm   = 1e99;
            disp('######### Newton Iterations ########');
            while (resNorm > 1e-15) && (nit <= 50)
                
                %% Evaluating relative permeability
                krw = obj.fluid.krw(sw,1:nc);
                krn = obj.fluid.krn(sw,1:nc);
            
                %% Mobility: Relative permeability over constant viscosity
                mobw = krw./obj.fluid_info.muw;
                mobn = krn./obj.fluid_info.mun;
                mobt = mobw + mobn;

                %% Pressure differences across each interface
                dp  = obj.operators.grad(pn);
                dpw = dp;

                %% Phase fluxes
                vW  = - obj.operators.upw(value(dpw) <= 0, mobt).*T.*dpw;

                %% Conservation of water and oil
                water = obj.operators.div(vW);

                %% BCs
                bc_fluxes = [];
                if(~isempty(obj.bc.cells))
                    [bc_fluxes] = compute_boundary_fluxes_single_phase(obj, sw, pn);
                    water(obj.bc.cells) = water(obj.bc.cells) - bc_fluxes;
                end

                %% Collect and concatenate all equations
                eq = water;

                %% Compute Newton update and update variable
                res = eq.val;
                upd = -(eq.jac{1} \ res);
                pn.val  = pn.val   + upd(pIx);
                
                resNorm = norm(res);
                disp(['Residual norm: ', num2str(resNorm)])
                nit     = nit + 1;
            end
            
            %% Appending results to state
            state.pn = value(pn);
            state.bc_fluxes = bc_fluxes;
        
        end
    end
end
        